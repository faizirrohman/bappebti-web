<!-- HEADER -->
<?php $this->load->view('include/header'); ?>
<!-- end: HEADER -->

<!-- BACKGROUND DTTOT 2 -->
<section class="parallax text-light halfscreen" data-bg-parallax="<?php echo base_url('assets/images/bg-all.png') ?>">
	<div class="container">
		<div class="container-fullscreen">
			<div class="text-middle text-center text-end">
				<h1 class="text-large" data-animate="animate__fadeInDown">Pemberitahuan Daftar Terduga Teroris dan Organisasi Teroris (DTTOT)</h1>
			</div>
		</div>
	</div>
</section>
<!-- end: BACKGROUND DTTOT 2 -->

<!-- PERATURAN, KEBIJAKAN, REKOMENDASI -->
<section class="background-grey">
	<div class="container">
		<div class="row">
			<div class="col-lg-4">
				<div class="heading-text">
					<h4>Peraturan DTTOT</h4>
				</div>
				<div class="post-thumbnail-list" style="overflow: scroll; height: 300px; overflow-x: hidden;">
					<div class="post-thumbnail-entry">
						<div class="post-thumbnail-content">
							<a href="#">DTTOT Nomor DTTOT/P-3a/96/VIII/RES.6.1./2019</a>
							<span class="post-date">28 Mei 2021</span>
						</div>
					</div>
					<div class="post-thumbnail-entry">
						<div class="post-thumbnail-content">
							<a href="#">Surat Pemberitahuan BAPPEBTI No 646.1/BAPPEBTI/SD/08/2019 Tentang Pemberitahuan  DTTOT No : DTTOT/P-3a/96/VIII/RES/6.1./2019</a>
							<span class="post-date">28 Mei 2021</span>
						</div>
					</div>
					<div class="post-thumbnail-entry">
						<div class="post-thumbnail-content">
							<a href="#">DTTOT Nomor: DTTOT/P-4b/105/II/RES.6.1./2020</a>
							<span class="post-date">28 Mei 2021</span>
						</div>
					</div>
					<div class="post-thumbnail-entry">
						<div class="post-thumbnail-content">
							<a href="#">Surat Pemberitahuan BAPPEBTI No. 48.1/BAPPEBTI/SD/02/2020 Tentang Pemberitahuan</a>
							<span class="post-date">28 Mei 2021</span>
						</div>
					</div>
				</div>
				<br>
				<a href="#" class="text-theme"><strong style="color: #589bba !important;">LIHAT SEMUA <i class="fa fa-angle-right "></strong></i></a>
				<br><br>
			</div>
			<div class="col-lg-4">
				<div class="heading-text">
					<h4>Kebijakan DTTOT</h4>
				</div>
				<div class="post-thumbnail-list" style="overflow: scroll; height: 300px; overflow-x: hidden;">
					<div class="post-thumbnail-entry">
						<div class="post-thumbnail-content">
							<a href="#">DTTOT Nomor DTTOT/P-3a/96/VIII/RES.6.1./2019</a>
							<span class="post-date">28 Mei 2021</span>
						</div>
					</div>
					<div class="post-thumbnail-entry">
						<div class="post-thumbnail-content">
							<a href="#">Surat Pemberitahuan BAPPEBTI No 646.1/BAPPEBTI/SD/08/2019 Tentang Pemberitahuan DTTOT No : DTTOT/P-3a/96/VIII/RES/6.1./2019</a>
							<span class="post-date">28 Mei 2021</span>
						</div>
					</div>
					<div class="post-thumbnail-entry">
						<div class="post-thumbnail-content">
							<a href="#">DTTOT Nomor: DTTOT/P-4b/105/II/RES.6.1./2020</a>
							<span class="post-date">28 Mei 2021</span>
						</div>
					</div>
					<div class="post-thumbnail-entry">
						<div class="post-thumbnail-content">
							<a href="#">Surat Pemberitahuan BAPPEBTI No. 48.1/BAPPEBTI/SD/02/2020 Tentang Pemberitah</a>
							<span class="post-date">28 Mei 2021</span>
						</div>
					</div>
				</div>
				<br>
				<a href="#" class="text-theme"><strong style="color: #589bba !important;">LIHAT SEMUA <i class="fa fa-angle-right "></strong></i></a>
				<br><br>
			</div>
			<div class="col-lg-4">
				<div class="heading-text">
					<h4>Rekomendasi Luar Negeri</h4>
				</div>
				<div class="post-thumbnail-list" style="overflow: scroll; height: 300px; overflow-x: hidden;">
					<div class="post-thumbnail-entry">
						<div class="post-thumbnail-content">
							<a href="#">DTTOT Nomor DTTOT/P-3a/96/VIII/RES.6.1./2019</a>
							<span class="post-date">28 Mei 2021</span>
						</div>
					</div>
					<div class="post-thumbnail-entry">
						<div class="post-thumbnail-content">
							<a href="#">Surat Pemberitahuan BAPPEBTI No 646.1/BAPPEBTI/SD/08/2019 Tentang Pemberitahuan DTTOT No : DTTOT/P-3a/96/VIII/RES/6.1./2019</a>
							<span class="post-date">28 Mei 2021</span>
						</div>
					</div>
					<div class="post-thumbnail-entry">
						<div class="post-thumbnail-content">
							<a href="#">DTTOT Nomor: DTTOT/P-4b/105/II/RES.6.1./2020</a>
							<span class="post-date">28 Mei 2021</span>
						</div>
					</div>
					<div class="post-thumbnail-entry">
						<div class="post-thumbnail-content">
							<a href="#">Surat Pemberitahuan BAPPEBTI No. 48.1/BAPPEBTI/SD/02/2020 Tentang Pemberitahuan DTTOT Nomor DTTOT/P-4b/105/II/RES.6.1./2020</a>
							<span class="post-date">28 Mei 2021</span>
						</div>
					</div>
				</div>
				<br>
				<a href="kegiatan.html" class="text-theme"><strong style="color: #589bba !important;">LIHAT SEMUA <i class="fa fa-angle-right "></strong></i></a>
			</div>
		</div>
	</div>
</section>
<!-- end: PERATURAN, KEBIJAKAN, REKOMENDASI -->

<!-- DTTOT -->
<section class="sidebar-right">
	<div class="container">
		<div class="heading-text">
			<h4>Daftar Terduga Teroris dan Organisasi Teroris (DTTOT)</h4>
		</div>
		<div class="row">
			<div class="content col-lg-10">
				<div id="blog">
					<div class="post-item">
						<div class="post-thumbnail-list">
							<div class="post-thumbnail">
								<div class="post-thumbnail-content">
									<p><strong>DTTOT11</strong></p>
								</div>
							</div>
							<hr>
							<div class="post-thumbnail">
								<div class="post-thumbnail-content">
									<p><strong>DTTOT12</strong></p>
								</div>
							</div>
							<hr>
							<div class="post-thumbnail">
								<div class="post-thumbnail-content">
									<p><strong>DTTOT13</strong></p>
								</div>
							</div>
							<hr>
							<div class="post-thumbnail">
								<div class="post-thumbnail-content">
									<p><strong>DTTOT1 4 (pdf)</strong></p>
								</div>
							</div>
							<hr>
							<div class="post-thumbnail">
								<div class="post-thumbnail-content">
									<p><strong>DTTOT1 4 (excel)</strong></p>
								</div>
							</div>
							<hr>
							<div class="post-thumbnail">
								<div class="post-thumbnail-content">
									<p><strong>DTTOT15</strong></p>
								</div>
							</div>
							<hr>
							<div class="post-thumbnail">
								<div class="post-thumbnail-content">
									<p><strong>DTTOT16</strong></p>
								</div>
							</div>
							<hr>
							<div class="post-thumbnail">
								<div class="post-thumbnail-content">
									<p><strong>DTTOT17</strong></p>
								</div>
							</div>
							<hr>
							<div class="post-thumbnail">
								<button class="btn btn-outline-primary">LIHAT SEMUA</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- end: DTTOT -->

<!-- FOOTER -->
<?php $this->load->view('include/footer'); ?>
<!-- end: FOOTER -->
