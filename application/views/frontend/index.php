<!-- HEADER -->
<?php $this->load->view('include/header'); ?>
<!-- end: HEADER -->

<div id="slider" class="inspiro-slider slider-halfscreen dots-dark arrows-dark dots-creative" data-fade="true" data-height-xs="360">
	<!-- SLIDE 1 -->
	<div class="slide background-image" data-bg-image="<?php echo base_url('assets/images/bg-home.jpg') ?>">
		<div class="container">
			<div class="slide-captions text-end">
				<h2 class="text-white text-lg">Bappebti, Aspebtindo, dan ICDX Selenggarakan P4WPB untuk Meningkatkan Kualitas Wakil Pialang Berjangka</h2>
			</div>
		</div>
	</div>
	<!-- end: SLIDE 1 -->

	<!-- SLIDE 2 -->
	<div class="slide background-image" data-bg-image="<?php echo base_url('assets/images/bg-home.jpg') ?>">
		<div class="container">
			<div class="slide-captions text-end">
				<h2 class="text-white text-lg">Bappebti, Aspebtindo, dan ICDX Selenggarakan P4WPB untuk Meningkatkan Kualitas Wakil Pialang Berjangka</h2>
			</div>
		</div>
	</div>
	<!-- end: SLIDE 2 -->
</div>

<!-- LAYANAN -->
<section id="page-content" style="background-color: #103A56;">
	<div class="container">
		<div class="grid-system-demo">
			<div class="row">
				<div class="col-lg-3"><span class="grid-col-demo">Harga Bursa 01 September 2021 ></span> </div>
				<div class="col-lg-9"><span class="grid-col-demo" style="color:white; background-color:#103A56; text-align: left;"> Komoditas TINPB100 -
						Jenis SPOT Jun21 - Lokasi Jakarta ICDX - Harga 0 US$/ton </span> </div>
			</div>
		</div>
		<div class="col-lg-12">
			<div class="row">
				<div class="col">
					<a href="https://play.google.com/store/apps/details?id=srgmobile.bappebti.go.id" target="blank">
						<img src="<?php echo base_url('assets/images/layanan/1.png') ?>" class="img-fluid rounded" alt="">
					</a>
				</div>
				<div class="col">
					<a href="http://ujianprofesi.bappebti.go.id/" target="blank">
						<img src="<?php echo base_url('assets/images/layanan/2.png') ?>" class="img-fluid rounded" alt="">
					</a>
				</div>
				<div class="col">
					<a href="http://infoharga.bappebti.go.id/" target="blank">
						<img src="<?php echo base_url('assets/images/layanan/3.png') ?>" class="img-fluid rounded" alt="">
					</a>
				</div>
				<div class="col">
					<a href=""><img src="<?php echo base_url('assets/images/layanan/4.png') ?>" class="img-fluid rounded" alt=""></a>
				</div>
				<div class="col">
					<a href="http://pengaduan.bappebti.go.id/" target="blank">
						<img src="<?php echo base_url('assets/images/layanan/5.png') ?>" class="img-fluid rounded" alt="">
					</a>
				</div>
				<div class="col">
					<a href="http://pbk.bappebti.go.id/" target="blank">
						<img src="<?php echo base_url('assets/images/layanan/6.png') ?>" class="img-fluid rounded" alt="">
					</a>
				</div>
				<div class="col">
					<a href="http://srg.bappebti.go.id/" target="blank">
						<img src="<?php echo base_url('assets/images/layanan/7.png') ?>" class="img-fluid rounded" alt="">
					</a>
				</div>
				<div class="col">
					<a href="http://plk.bappebti.go.id:93/" target="blank">
						<img src="<?php echo base_url('assets/images/layanan/8.png') ?>" class="img-fluid rounded" alt="">
					</a>
				</div>
				<div class="col">
					<a href="https://play.google.com/store/apps/details?id=pasarlelangterpadu.com.explorindo.app.pasarlelangterpaduv2" target="blank">
						<img src="<?php echo base_url('assets/images/layanan/9.png') ?>" class="img-fluid rounded" alt="">
					</a>
				</div>
			</div>
		</div>
		<!-- <ul class="list-unstyled mb-0 row gutters-5">
			<li class="minw-0 col-4 col-md mt-3">
				<a href="" class="d-block rounded bg-white p-2 text-reset shadow-sm">
					<img src="img/layanan/1.png" class="img-fluid" alt="">
				</a>
			</li>
			<li class="minw-0 col-4 col-md mt-3">
				<a href="" class="d-block rounded bg-white p-2 text-reset shadow-sm">
					<img src="img/layanan/2.png" class="img-fluid" alt="">
				</a>
			</li>
			<li class="minw-0 col-4 col-md mt-3">
				<a href="" class="d-block rounded bg-white p-2 text-reset shadow-sm">
					<img src="img/layanan/3.png" class="img-fluid" alt="">
				</a>
			</li>
			<li class="minw-0 col-4 col-md mt-3">
				<a href="" class="d-block rounded bg-white p-2 text-reset shadow-sm">
					<img src="img/layanan/4.png" class="img-fluid" alt="">
				</a>
			</li>
			<li class="minw-0 col-4 col-md mt-3">
				<a href="" class="d-block rounded bg-white p-2 text-reset shadow-sm">
					<img src="img/layanan/5.png" class="img-fluid" alt="">
				</a>
			</li>
			<li class="minw-0 col-4 col-md mt-3">
				<a href="" class="d-block rounded bg-white p-2 text-reset shadow-sm">
					<img src="img/layanan/6.png" class="img-fluid" alt="">
				</a>
			</li>
			<li class="minw-0 col-4 col-md mt-3">
				<a href="" class="d-block rounded bg-white p-2 text-reset shadow-sm">
					<img src="img/layanan/7.png" class="img-fluid" alt="">
				</a>
			</li>
			<li class="minw-0 col-4 col-md mt-3">
				<a href="" class="d-block rounded bg-white p-2 text-reset shadow-sm">
					<img src="img/layanan/8.png" class="img-fluid" alt="">
				</a>
			</li>
			<li class="minw-0 col-4 col-md mt-3">
				<a href="" class="d-block rounded bg-white p-2 text-reset shadow-sm">
					<img src="img/layanan/9.png" class="img-fluid" alt="">
				</a>
			</li>
		</ul> -->
	</div>
</section>
<!-- end: LAYANAN -->

<section class="background-grey">
	<div class="container">
		<div id="blog" class="grid-layout post-3-columns m-b-30" data-item="post-item">
			<div class="post-item border">
				<div class="post-item-wrap">
					<div class="post-image">
						<a href="#">
							<img alt="" style="height: 200px;" src="<?php echo base_url('assets/images/12.jpg') ?>" class="img-fluid rounded">
						</a>
						<span class="post-meta-category" style="top: 160px; right: 238px;"><a href="">Learn
								more</a></span>
					</div>
				</div>
			</div>
			<div class="post-item border">
				<div class="post-item-wrap">
					<div class="post-image">
						<a href="#">
							<img alt="" style="height: 200px;" src="<?php echo base_url('assets/images/12.jpg') ?>" class="img-fluid rounded">
						</a>
						<span class="post-meta-category" style="top: 160px; right: 238px;"><a href="">Learn
								more</a></span>
					</div>
				</div>
			</div>
			<div class="post-item border">
				<div class="post-item-wrap">
					<div class="post-image">
						<a href="#">
							<img alt="" style="height: 200px;" src="<?php echo base_url('assets/images/12.jpg') ?>" class="img-fluid rounded">
						</a>
						<span class="post-meta-category" style="top: 160px; right: 238px;"><a href="">Learn
								more</a></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- SIARAN PERS -->
<section id="page-content">
	<div id="blog">
		<div class="container">
			<div id="blog">
				<div class="container">
					<div class="row ">
						<div class="col-lg-6">
							<div class="heading-text">
								<h4>Siaran Pers</h4>
							</div>
						</div>
						<div class="col-lg-6" style="text-align: right;">
							<div class="heading-text">
								<a href="<?php echo base_url('berita/siaran-pers') ?>"><strong style="color: #589bba !important; font-size: 15px;">SEMUA SIARAN PERS <i class="fa fa-angle-right "></i></strong></a>
							</div>
						</div>
					</div>
				</div>
				<div id="blog" class="grid-layout post-4-columns m-b-30" data-item="post-item">
					<div class="post-item">
						<div class="post-item-wrap">
							<div class="post-image">
								<a href="<?php echo base_url('berita/siaran-pers/detail-siaran-pers') ?>">
									<img alt="" src="<?php echo base_url('assets/images/siaran-pers/2.png') ?>">
								</a>
							</div>
							<div class="post-item-description">
								<div>
									<span type="button" class="btn btn-primary btn-sm">Siaran Pers</span>
								</div>
								<h2>
									<a href="<?php echo base_url('berita/siaran-pers/detail-siaran-pers') ?>">Pencabutan Izin Usaha Untuk Menyelenggarakan Kegiatan Sebag...</a>
								</h2>
								<span class="post-meta-date"><i class="fa fa-calendar-o"></i>28 Mei 2021</span>
							</div>
						</div>
					</div>
					<div class="post-item">
						<div class="post-item-wrap">
							<div class="post-image">
								<a href="<?php echo base_url('berita/siaran-pers/detail-siaran-pers') ?>">
									<img alt="" src="<?php echo base_url('assets/images/siaran-pers/3.png') ?>">
								</a>
							</div>
							<div class="post-item-description">
								<div>
									<span type="button" class="btn btn-primary btn-sm">Siaran Pers</span>
								</div>
								<h2>
									<a href="<?php echo base_url('berita/siaran-pers/detail-siaran-pers') ?>">Pencabutan Izin Usaha Untuk Menyelenggarakan Kegiatan Sebag...</a>
								</h2>
								
								<span class="post-meta-date"><i class="fa fa-calendar-o"></i>28 Mei 2021</span>
							</div>
						</div>
					</div>
					<div class="post-item">
						<div class="post-item-wrap">
							<div class="post-image">
								<a href="<?php echo base_url('berita/siaran-pers/detail-siaran-pers') ?>">
									<img alt="" src="<?php echo base_url('assets/images/siaran-pers/4.png') ?>">
								</a>
							</div>
							<div class="post-item-description">
								<div>
									<span type="button" class="btn btn-primary btn-sm">Siaran Pers</span>
								</div>
								<h2>
									<a href="<?php echo base_url('berita/siaran-pers/detail-siaran-pers') ?>">Pencabutan Izin Usaha Untuk Menyelenggarakan Kegiatan Sebag...</a>
								</h2>
								<span class="post-meta-date"><i class="fa fa-calendar-o"></i>28 Mei 2021</span>
							</div>
						</div>
					</div>
					<div class="post-item">
						<div class="post-item-wrap">
							<div class="post-image">
								<a href="<?php echo base_url('berita/siaran-pers/detail-siaran-pers') ?>">
									<img alt="" src="<?php echo base_url('assets/images/siaran-pers/5.png') ?>">
								</a>
							</div>
							<div class="post-item-description">
								<div>
									<span type="button" class="btn btn-primary btn-sm">Siaran Pers</span>
								</div>
								<h2>
									<a href="<?php echo base_url('berita/siaran-pers/detail-siaran-pers') ?>">Pencabutan Izin Usaha Untuk Menyelenggarakan Kegiatan Sebag...</a>
								</h2>

								<span class="post-meta-date"><i class="fa fa-calendar-o"></i>28 Mei 2021</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- end: SIARAN PERS -->

<!-- AKTUALITA, PENGATURAN BARU, KEGIATAN -->
<section class="background-grey">
	<div class="container">
		<div class="row">
			<div class="col-lg-4">
				<div class="heading-text">
					<h4>Aktualita</h4>
				</div>
				<div class="post-thumbnail-list" style="overflow: scroll; height: 300px; overflow-x: hidden;">
					<div class="post-thumbnail-entry">
						<div class="post-thumbnail-content">
							<a href="#">Pengumuman Pelaksanaan Ujian Profesi Calon Wakil Pialang Berjangka 8-9 Juli 2021 Di Bandung</a>
							<span class="post-date">28 Mei 2021</span>
						</div>
					</div>
					<div class="post-thumbnail-entry">
						<div class="post-thumbnail-content">
							<a href="#">Pengumuman Nama Peserta Yang Berhak Mengikuti Ujian Profesi Calon Wakil Pialang Perdagangan Berjangka Komoditi Di Jakarta, 29...</a>
							<span class="post-date">28 Mei 2021</span>
						</div>
					</div>
					<div class="post-thumbnail-entry">
						<div class="post-thumbnail-content">
							<a href="#">Pengumuman Pelaksanaan Ujian Profesi Calon Wakil Pialang Berjangka Periode April 2021</a>
							<span class="post-date">28 Mei 2021</span>
						</div>
					</div>
					<div class="post-thumbnail-entry">
						<div class="post-thumbnail-content">
							<a href="#">Pengumuman Pelaksanaan Ujian Profesi Calon Wakil Pialang Berjangka Periode April 2021</a>
							<span class="post-date">28 Mei 2021</span>
						</div>
					</div>
					<div class="post-thumbnail-entry">
						<div class="post-thumbnail-content">
							<a href="#">Pengumuman Pelaksanaan Ujian Profesi Calon Wakil Pialang Berjangka Periode April 2021</a>
							<span class="post-date">28 Mei 2021</span>
						</div>
					</div>
				</div>
				<br>
				<a href="#" class="text-theme"><strong style="color: #589bba !important;">LIHAT SEMUA <i class="fa fa-angle-right "></strong></i></a>
				<br><br>
			</div>
			<div class="col-lg-4">
				<div class="heading-text">
					<h4>Peraturan Baru</h4>
				</div>
				<div class="post-thumbnail-list" style="overflow: scroll; height: 300px; overflow-x: hidden;">
					<div class="post-thumbnail-entry">
						<div class="post-thumbnail-content">
							<a href="#">Peraturan Bappebti</a>
							<span class="post-date">28 Mei 2021</span>
						</div>
					</div>
					<div class="post-thumbnail-entry">
						<div class="post-thumbnail-content">
							<a href="#">Nomor 4 Tahun 2021</a>
							<span class="post-date">28 Mei 2021</span>
						</div>
					</div>
					<div class="post-thumbnail-entry">
						<div class="post-thumbnail-content">
							<a href="#">Peraturan Menteri Perdagangan Republik Indonesia Nomor 14 Tahun 2021</a>
							<span class="post-date">28 Mei 2021</span>
						</div>
					</div>
					<div class="post-thumbnail-entry">
						<div class="post-thumbnail-content">
							<a href="#">Peraturan Menteri Perdagangan Republik Indonesia Nomor 14 Tahun 2021</a>
							<span class="post-date">28 Mei 2021</span>
						</div>
					</div>
					<div class="post-thumbnail-entry">
						<div class="post-thumbnail-content">
							<a href="#">Peraturan Menteri Perdagangan Republik Indonesia Nomor 14 Tahun 2021</a>
							<span class="post-date">28 Mei 2021</span>
						</div>
					</div>
				</div>
				<br>
				<a href="<?php echo base_url('dttot') ?>" class="text-theme"><strong style="color: #589bba !important;">LIHAT SEMUA <i class="fa fa-angle-right "></strong></i></a>
				<br>
				<br>
			</div>
			<div class="col-lg-4">
				<div class="heading-text">
					<h4>Kegiatan</h4>
				</div>
				<div class="post-thumbnail-list" style="overflow: scroll; height: 300px; overflow-x: hidden;">
					<div class="post-thumbnail-entry">
						<div class="post-thumbnail-content">
							<a href="#">Bertemu Bupati Batubara, Wamendag Dorong Implementasi Srg untuk Tingkatkan Kesejahteraan Petani</a>
							<span class="post-date">28 Mei 2021</span>
						</div>
					</div>
					<div class="post-thumbnail-entry">
						<div class="post-thumbnail-content">
							<a href="#">Dukungan Rantai Bisnis Produk Perikanan, Kemendag Optimalkan Dan Perluas Pemanfaatan Srg</a>
							<span class="post-date">28 Mei 2021</span>
						</div>
					</div>
					<div class="post-thumbnail-entry">
						<div class="post-thumbnail-content">
							<a href="#">Gerakkan Perekonomian Nasional Dan Tingkatkan Kesejahteraan, Pemerintah Dorong Penggunaan Srg</a>
							<span class="post-date">28 Mei 2021</span>
						</div>
					</div>
				</div>
				<br>
				<a href="<?php echo base_url('kegiatan') ?>" class="text-theme"><strong style="color: #589bba !important;">LIHAT SEMUA <i class="fa fa-angle-right "></strong></i></a>
				<br><br>
			</div>
		</div>
		<br>
		<br>
		<div class="row">
			<div class="col-lg-12">
				<div class="post-thumbnail-list">
					<div class="post-image">
						<img alt="" class="img-fluid" src="<?php echo base_url('assets/images/1200x90.png') ?>">
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- end: AKTUALITA, PENGATURAN BARU, KEGIATAN -->

<!-- POJOK MEDIA -->
<section id="page-content">
	<div id="blog">
		<div class="container">
			<div id="blog">
				<div class="container">
					<div class="row ">
						<div class="col-lg-6">
							<div class="heading-text">
								<h4>Pojok Media</h4>
							</div>
						</div>
						<div class="col-lg-6" style="text-align: right;">
							<div class="heading-text">
								<a href="#"><strong style="color: #589bba !important; font-size: 15px;">SEMUA POJOK MEDIA <i class="fa fa-angle-right "></i></strong></a>
							</div>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-lg-6">
							<div class="post-item">
								<div class="post-item-wrap">
									<div class="post-image">
										<a href="#">
											<img alt="" src="<?php echo base_url('assets/images/pojok-media/5.png') ?>">
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div id="blog" class="grid-layout post-2-columns m-b-30" data-item="post-item">
								<div class="post-item">
									<div class="post-item-wrap">
										<div class="post-image">
											<a href="#">
												<img alt="Responsive image" src="<?php echo base_url('assets/images/pojok-media/1.jpg') ?>">
											</a>
										</div>
										<div class="post-item-description">
											<div>
												<span type="button" class="btn btn-primary btn-sm">Pojok Media</span>
											</div>
											<h2><a href="#">Pengumuman Nama Peserta Yang Berhak Mengikuti Ujia...</a></h2>
											<span class="post-meta-date"><i class="fa fa-calendar-o"></i>28 Mei 2021</span>
										</div>
									</div>
								</div>
								<div class="post-item">
									<div class="post-item-wrap">
										<div class="post-image">
											<a href="#">
												<img alt="" src="<?php echo base_url('assets/images/pojok-media/2.jpg') ?>">
											</a>
										</div>
										<div class="post-item-description">
											<div>
												<span type="button" class="btn btn-primary btn-sm">Pojok Media</span>
											</div>
											<h2><a href="#">Pengumuman Nama Peserta Yang Berhak Mengikuti Ujia...</a></h2>
											<span class="post-meta-date"><i class="fa fa-calendar-o"></i>28 Mei 2021</span>
										</div>
									</div>
								</div>
								<div class="post-item">
									<div class="post-item-wrap">
										<div class="post-image">
											<a href="#">
												<img alt="" src="<?php echo base_url('assets/images/pojok-media/3.jpg') ?>">
											</a>
										</div>
										<div class="post-item-description">
											<div>
												<span type="button" class="btn btn-primary btn-sm">Pojok Media</span>
											</div>
											<h2><a href="#">Pengumuman Nama Peserta Yang Berhak Mengikuti Ujia...</a></h2>
											<span class="post-meta-date"><i class="fa fa-calendar-o"></i>28 Mei 2021</span>
										</div>
									</div>
								</div>
								<div class="post-item">
									<div class="post-item-wrap">
										<div class="post-image">
											<a href="#">
												<img alt="" src="<?php echo base_url('assets/images/pojok-media/4.jpg') ?>">
											</a>
										</div>
										<div class="post-item-description">
											<div>
												<span type="button" class="btn btn-primary btn-sm">Pojok Media</span>
											</div>
											<h2><a href="#">Pengumuman Nama Peserta Yang Berhak Mengikuti Ujia...</a></h2>
											<span class="post-meta-date"><i class="fa fa-calendar-o"></i>28 Mei 2021</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- end: POJOK MEDIA -->

<!-- PUBLIKASI -->
<section class="background-grey">
	<div class="container">
		<div class="heading-text">
			<h4>Publikasi</h4>
		</div>
		<div class="col-12">
			<div id="blog" class="grid-layout post-3-columns m-b-30" data-item="post-item">
				<div class="post-item">
					<div class="post-item-wrap" style="border-radius: 10px;">
						<div class="post-item-description" style="padding: 28px;">
							<h2>Annual Report</h2><br>
							<a href="#">
								<img alt="" style="height: 400px;" src="<?php echo base_url('assets/images/publikasi/1.png') ?>">
							</a>
							<br><br>
							<p>Annual Report Bappebti Tahun 2021</p>
							<button class="btn btn-outline-primary">LIHAT SEMUA</button>
						</div>
					</div>
				</div>
				<div class="post-item">
					<div class="post-item-wrap" style="border-radius: 10px;">
						<div class="post-item-description" style="padding: 28px;">
							<h2>Bulletin Berjangka</h2><br>
							<a href="#">
								<img alt="" style="height: 400px;" src="<?php echo base_url('assets/images/publikasi/2.png') ?>">
							</a><br><br>
							<p>Timah Bergairah di Dua Bursa Komoditi</p>
							<button class="btn btn-outline-primary">LIHAT SEMUA</button>
						</div>
					</div>
				</div>
				<div class="post-item">
					<div class="post-item-wrap" style="border-radius: 10px;">
						<div class="post-item-description" style="padding: 28px;">
							<h2>Perdagangan Berjangka</h2><br>
							<a href="#">
								<img alt="" style="height: 400px;" src="<?php echo base_url('assets/images/publikasi/3.png') ?>">
							</a><br><br>
							<p>Buku Analisa Perkembangan Harga Komoditi Triwulan 4 2020</p>
							<a href="bulletin-perdagangan-berjangka.html" class="btn btn-outline-primary">LIHAT SEMUA</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-12">
			<div class="row">
				<div id="blog" class="grid-layout post-2-columns m-b-20" data-item="post-item">
					<div class="col-5">
						<div class="post-item">
							<div class="post-item-wrap" style="border-radius: 10px;">
								<div class="post-item-description" style="padding: 28px;">
									<h2>Media Sosial</h2>
									<a href="#">
										<img alt="" src="<?php echo base_url('assets/images/publikasi/sosmed.png') ?>">
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-7">
						<div class="post-item">
							<div class="post-item-wrap" style="border-radius: 10px;">
								<div class="post-item-description" style="padding: 28px;">
									<h2>Kurs Tengah</h2>
									<a href="#">
										<img alt="" src="<?php echo base_url('assets/images/publikasi/chart.png') ?>">
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- end: PUBLIKASI -->

<!-- KERJA SAMA BAPPEBTI -->
<section id="page-content">
	<div class="container">
		<div class="row">
			<div class="content col-lg-12">
				<div class="heading-text">
					<h4>Kerja Sama Bappebti</h4>
				</div>
				<div class="carousel client-logos" data-items="5" data-dots="false"> 
					<div>
						<a href="http://www.aspebtindo.org/" target="blank">
							<img alt="" src="<?php echo base_url('assets/images/clients/client-1.png') ?>">
						</a>
					</div>
					<div>
						<a href="http://www.jfx.co.id/id" target="blank">
							<img alt="" src="<?php echo base_url('assets/images/clients/client-2.png') ?>">
						</a>
					</div>
					<div>
						<a href="http://www.icdx.co.id/" target="blank">
							<img alt="" src="<?php echo base_url('assets/images/clients/client-3.png') ?>">
						</a>
					</div>
					<div>
						<a href="http://www.ptkbi.com/" target="blank">
							<img alt="" src="<?php echo base_url('assets/images/clients/client-4.png') ?>">
						</a>
					</div>
					<div>
						<a href="http://www.ich.co.id/" target="blank">
							<img alt="" src="<?php echo base_url('assets/images/clients/client-5.jpg') ?>">
						</a>
					</div>
					<div>
						<a href="http://www.bakti-arb.org/" target="blank">
							<img alt="" src="<?php echo base_url('assets/images/clients/client-6.png') ?>">
						</a>
					</div>
					<div>
						<a href="http://www.kemendag.go.id/" target="blank">
							<img alt="" src="<?php echo base_url('assets/images/clients/client-7.png') ?>">
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- end: KERJA SAMA BAPPEBTI -->

<!-- TONTON APA YANG KAMI BUAT UNTUK ANDA -->
<section class="sidebar-right" style="background-color: rgb(2 39 78);">
	<div class="container">
		<div class="heading-text">
			<h4 style="color: white;">Tonton apa yang kami buat untuk Anda</h4>
		</div>
		<div class="row">
			<div class="content col-lg-9">
				<div class="post-item border">
					<div class="post-item-wrap" style="background-color: #103A56; border-radius: 9px; border: #103A56 !important;">
						<div class="post-video">
							<div class="ratio ratio-16x9">
								<iframe class="responsive-iframe rounded-top" width="560" height="376" src="https://www.youtube.com/embed/tgbNymZ7vqY" frameborder="0" allowfullscreen></iframe>
							</div>
						</div>
						<div class="post-item-description">
							<h2><a style="color: white;" href="#">Cuan Saham vs Aset Crypto, mana yang lebih menjanjikan ?</a></h2>
							<span class="post-meta-date"><i class="fa fa-calendar-o"></i>31 Mei 2021</span>
						</div>
					</div>
				</div>
			</div>

			<!-- SIDEBAR RIGHT-->
			<div class="sidebar col-lg-3" style="overflow: auto; height: 580px;">
				<div class="post-item border">
					<div class="post-item-wrap" style="background-color: #103A56; border-radius: 9px; border: #103A56 !important;">
						<div class="post-video">
							<div class="ratio ratio-16x9">
								<iframe class="responsive-iframe rounded-top" width="560" height="376" src="https://www.youtube.com/embed/tgbNymZ7vqY" frameborder="0" allowfullscreen></iframe>
							</div>
						</div>
						<div class="post-item-description">
							<h5><a href="#" style="color: white;">Creative Money, Jurus Investasi</a></h5>
							<br>
							<span class="post-meta-date"><i class="fa fa-calendar-o"></i>20 April 2021</span>
						</div>
					</div>
				</div>
				<div class="post-item border">
					<div class="post-item-wrap" style="background-color: #103A56; border-radius: 9px; border: #103A56 !important;">
						<div class="post-video">
							<div class="ratio ratio-16x9">
								<iframe class="responsive-iframe rounded-top" width="560" height="376" src="https://www.youtube.com/embed/tgbNymZ7vqY" frameborder="0" allowfullscreen></iframe>
							</div>
						</div>
						<div class="post-item-description">
							<h5><a href="#" style="color: white;">Investasi Aset Crypto: Ngeri atau Sedap?</a></h5>
							<br>
							<span class="post-meta-date"><i class="fa fa-calendar-o"></i>20 April 2021</span>
						</div>
					</div>
				</div>
				<div class="post-item border">
					<div class="post-item-wrap" style="background-color: #103A56; border-radius: 9px; border: #103A56 !important;">
						<div class="post-video">
							<div class="ratio ratio-16x9">
								<iframe class="responsive-iframe rounded-top" width="560" height="376" src="https://www.youtube.com/embed/tgbNymZ7vqY" frameborder="0" allowfullscreen></iframe>
							</div>
						</div>
						<div class="post-item-description">
							<h5><a href="#" style="color: white;">Investasi Aset Crypto Menggiurkan, Bagaimana P...</a></h5>
							<br>
							<span class="post-meta-date"><i class="fa fa-calendar-o"></i>20 April 2021</span>
						</div>
					</div>
				</div>
			</div>
			<!-- end: SIDEBAR RIGHT-->
		</div>
	</div>
</section>
<!-- end: TONTON APA YANG KAMI BUAT UNTUK ANDA -->

<!-- INFORMASI BAPPEBTI -->
<section id="page-content">
	<div class="container">
		<div class="row ">
			<div class="col-lg-6">
				<div class="heading-text">
					<h4>Informasi Bappebti</h4>
				</div>
			</div>
		</div>  
		<div id="portfolio" class="grid-layout portfolio-3-columns" data-margin="20">
			<div class="portfolio-item no-overlay img-zoom">
				<div class="portfolio-item-wrap">
					<div class="portfolio-image">
						<a href="#"><img src="<?php echo base_url('assets/images/infobappebti/1.png') ?>" alt=""></a>
					</div>
				</div>
			</div>
			<div class="portfolio-item no-overlay img-zoom">
				<div class="portfolio-item-wrap">
					<div class="portfolio-image">
						<a href="#"><img src="<?php echo base_url('assets/images/infobappebti/2.png') ?>" alt=""></a>
					</div>
				</div>
			</div>
			<div class="portfolio-item no-overlay img-zoom">
				<div class="portfolio-item-wrap">
					<div class="portfolio-image">
						<a href="#"><img src="<?php echo base_url('assets/images/infobappebti/3.png') ?>" alt=""></a>
					</div>
				</div>
			</div>

			<div class="portfolio-item no-overlay img-zoom">
				<div class="portfolio-item-wrap">
					<div class="portfolio-image">
						<a href="#"><img src="<?php echo base_url('assets/images/infobappebti/4.png') ?>" alt=""></a>
					</div>
				</div>
			</div>
			<div class="portfolio-item no-overlay img-zoom">
				<div class="portfolio-item-wrap">
					<div class="portfolio-image">
						<a href="#"><img src="<?php echo base_url('assets/images/infobappebti/5.png') ?>" alt=""></a>
					</div>
				</div>
			</div>
			<div class="portfolio-item no-overlay img-zoom">
				<div class="portfolio-item-wrap">
					<div class="portfolio-image">
						<a href="#"><img src="<?php echo base_url('assets/images/infobappebti/6.png') ?>" alt=""></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- end: INFORMASI BAPPEBTI -->

<!-- FOOTER -->
<?php $this->load->view('include/footer'); ?>
<!-- end: FOOTER -->
