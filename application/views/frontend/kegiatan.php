<!-- HEADER -->
<?php $this->load->view('include/header'); ?>
<!-- end: HEADER -->

<!-- KEGIATAN -->
<section class="sidebar-right background-grey">
	<div class="container">
		<div class="heading-text">
			<!-- BREADCRUMB -->
			<div class="breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="#">Halaman Utama</a></li>
						<li class="breadcrumb-item active" aria-current="page">Kegiatan</li>
					</ol>
				</nav>
			</div>
			<!-- end: BREADCRUMB -->
			<hr>
			<h4>Kegiatan</h4>
		</div>
		<div class="row">
			<div class="content col-lg-8">
				<form action="" method="post">
					<div class="row">
						<div class="col-lg-5">
							<input type="text" name="" class="form-control background-grey" placeholder="Kata Kunci..">
						</div>
						<div class="col-lg-2">
							<select name="" id="" class="form-control background-grey">
								<option selected>All</option>
							</select>
						</div>
						<div class="col-lg-2">
							<button type="submit" id="btn-search" class="form-control btn btn-primary"><i class="icon-search"></i></button>
						</div>
					</div>
				</form>
				<br>
				<div id="blog">
					<div class="post-item">
						<div class="post-thumbnail-list">
							<div class="post-thumbnail-entry">
								<div class="post-thumbnail-content">
									<span type="button" class="btn btn-primary btn-sm">2021</span>
									<a href="">
										<p><strong>Bertemu Bupati Batubara, Wamendag Dorong Implementasi Srg untuk Tingkatkan Kesejahteraan Petani</strong></p>
									</a>
									<span class="post-date">21 Mei 2021</span>
								</div>
							</div>
							<div class="post-thumbnail-entry">
								<div class="post-thumbnail-content">
									<span type="button" class="btn btn-primary btn-sm">2021</span>
									<a href="">
										<p><strong>Dukung Rantai Bisnis Produk Perikanan, Kemendag Optimalkan Dan Perluas Pemanfaatan Srg</strong></p>
									</a>
									<span class="post-date">21 Mei 2021</span>
								</div>
							</div>
							<div class="post-thumbnail-entry">
								<div class="post-thumbnail-content">
									<span type="button" class="btn btn-primary btn-sm">2021</span>
									<a href="">
										<p><strong>Dukung Rantai Bisnis Produk Perikanan, Kemendag Optimalkan Dan Perluas Pemanfaatan Srg</strong></p>
									</a>
									<span class="post-date">21 Mei 2021</span>
								</div>
							</div>
							<div class="post-thumbnail-entry">
								<div class="post-thumbnail-content">
									<span type="button" class="btn btn-primary btn-sm">2021</span>
									<a href="">
										<p><strong>Dukung Rantai Bisnis Produk Perikanan, Kemendag Optimalkan Dan Perluas Pemanfaatan Srg</strong></p>
									</a>
									<span class="post-date">21 Mei 2021</span>
								</div>
							</div>
							<div class="post-thumbnail-entry">
								<div class="post-thumbnail-content">
									<span type="button" class="btn btn-primary btn-sm">2021</span>
									<a href="">
										<p><strong>Dukung Rantai Bisnis Produk Perikanan, Kemendag Optimalkan Dan Perluas Pemanfaatan Srg</strong></p>
									</a>
									<span class="post-date">21 Mei 2021</span>
								</div>
							</div>
							<div class="post-thumbnail">
								<button class="btn btn-outline-primary">LIHAT SEMUA</button>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- SIDEBAR RIGHT-->
			<div class="sidebar col-lg-4">
				<div class="widget ">
					<div class="post-thumbnail-list">
						<div class="post-thumbnail-entry">
							<div class="post-image">
								<a href="#">
									<img alt="" src="<?php echo base_url('assets/images/1.png') ?>">
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- end: SIDEBAR RIGHT-->
		</div>
	</div>
</section>
<!-- end: KEGIATAN -->

<!-- FOOTER -->
<?php $this->load->view('include/footer'); ?>
<!-- end: FOOTER -->
