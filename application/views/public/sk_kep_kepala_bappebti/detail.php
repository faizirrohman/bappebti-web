<!-- HEADER -->
<?php $this->load->view('include/header'); ?>
<!-- end: HEADER -->

<!-- KEPUTUSAN KEPALA BAPPEBTI -->
<section class="sidebar-right background-grey">
	<div class="container">
		<div class="heading-text">
			<!-- BREADCRUMB -->
			<div class="breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html">Halaman Utama</a></li>
						<li class="breadcrumb-item"><a href="#">PBK</a></li>
						<li class="breadcrumb-item"><a href="pbk-kep-kepala-bappebti.html">SK/ Kep. Kepala Bappebti</a></li>
						<li class="breadcrumb-item active" aria-current="page">Detail SK/ Kep. Kepala Bappebti</li>
					</ol>
				</nav>
			</div>
			<!-- end: BREADCRUMB -->
		</div>
		<div class="row">
			<div class="content col-lg-8">
				<div id="blog" class="grid-layout post-1-columns m-b-30" data-item="post-item">
					<div class="post-item">
						<div style="border: 1px solid rgb(205, 205, 205);">
							<div class="post-item-description" style="padding: 35px;">
								<h2>Nomor 8 Tahun 2021</h2>
								<hr><br>
									<iframe src='<?php echo base_url('assets/pdf/pbk/kep_kepala_bappebti.pdf') ?>' width="650" height="500"></iframe>
									<p>Jika Load PDF Gagal. Klik Link Ini untuk <a href="" target="blank"><strong style="color: black;">Download PDF </strong></a></p>
								<br><hr><br>
								<div class="col-12">
									<div class="row">
										<div class="col-6">
											<strong style="color: black;">AUTHOR :</strong> <span style="color: blue;">Admin</span>
										</div>
										<div class="col-6">
											<strong style="color: black;">TAGS</strong>
										</div>
									</div>
									<div class="row">
										<div class="col-6">
											<strong style="color: black;">TENTANG :</strong> <span>Pedoman Penyelenggaraan Perdagangan Pasar Fisik Aset Kripto (Crypto Asset) di Bursa Berjangka</span>
										</div>
										<div class="col-6">
											<button style="color: grey; border: 0px">SK/ Kep. Kepala Bappebti</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- SIDEBAR RIGHT-->
			<div class="sidebar col-lg-4">
				<div class="widget">
					<div class="post-thumbnail-list">
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<span type="button" class="btn btn-primary btn-sm">SK/ Kep. Kepala Bappebti Terpopuler</span>
								<br><br>
								<a href="#">Nomor 4 Tahun 2018</a>
								<span class="post-date">01 Agustus 2018</span>
								<span class="post-category">Views : 6817</span>
							</div>
						</div>
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<a href="#">Nomor 5 Tahun 2019</a>
								<span class="post-date">01 Februari 2019</span>
								<span class="post-category">Views : 5870</span>
							</div>
						</div>
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<a href="#">Nomor 5 Tahun 2017</a>
								<span class="post-date">30 Mei 2017</span>
								<span class="post-category">Views : 5642</span>
							</div>
						</div>
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<a href="#">Nomor 8 Tahun 2019</a>
								<span class="post-date">01 Juli 2019</span>
								<span class="post-category">Views : 5551</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- end: SIDEBAR RIGHT-->
		</div>
	</div>
</section>
<!-- end: KEPUTUSAN KEPALA BAPPEBTI -->

<!-- FOOTER -->
<?php $this->load->view('include/footer'); ?>
<!-- end: FOOTER -->
