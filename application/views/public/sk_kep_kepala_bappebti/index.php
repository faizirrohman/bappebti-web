<!-- HEADER -->
<?php $this->load->view('include/header'); ?>
<!-- end: HEADER -->

<!-- KEPUTUSAN KEPALA BAPPEBTI -->
<section class="sidebar-right background-grey">
	<div class="container">
		<div class="heading-text">
			<!-- BREADCRUMB -->
			<div class="breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="#">Halaman Utama</a></li>
						<li class="breadcrumb-item"><a href="#">PBK</a></li>
						<li class="breadcrumb-item active" aria-current="page">SK/ Kep. Kepala Bappebti</li>
					</ol>
				</nav>
			</div>
			<!-- end: BREADCRUMB -->
			<hr>
			<h4>SK/ Kep. Kepala Bappebti</h4>
		</div>
		<div class="row">
			<div class="content col-lg-8">
				<form action="" method="post">
					<div class="row">
						<div class="col-lg-5">
							<input type="text" name="" class="form-control background-grey" placeholder="Judul">
						</div>
						<div class="col-lg-2">
							<button type="submit" id="btn-search" class="form-control" style="background-color: #6cb6d8;"><i style="color: white;" class="icon-search"></i></button>
						</div>
					</div>
				</form>
				<br>
				<div id="blog">
					<div class="post-item">
						<div class="post-thumbnail-list">
							<div class="post-thumbnail">
								<div class="post-thumbnail-content">
									<span class="btn btn-primary btn-sm">01 November 2021</span>
									<a href="<?php echo base_url('pbk/sk_kep_kepala_bappebti/detail') ?>">
										<p><strong>Nomor 8 Tahun 2021</strong></p>
										<p style="color: #3c4043;">Tupoksi : Perdagangan Berjangka Komoditi</p>
									</a>
									<p>Tentang : Pedoman Penyelenggaraan Perdagangan Pasar Fisik Aset Kripto (Crypto Asset) di Bursa Berjangka</p>
								</div>
							</div>
							<hr>
							<div class="post-thumbnail">
								<div class="post-thumbnail-content">
									<span class="btn btn-primary btn-sm">01 September 2021</span>
									<a href="#">
										<p><strong>Nomor 6 Tahun 2021</strong></p>
										<p style="color: #3c4043;">Tupoksi : Perdagangan Berjangka Komoditi</p>
									</a>
									<p>Tentang : Perubahan Atas Peraturan Kepala Badan Pengawas Perdagangan Berjangka Komoditi Nomor 106/BAPPEBTI/PER/10/20213 Tentang Kewajiban Pelaporan Keuangan dan Ketentuan Modal Bersih Disesuaikan Bagi Pialang Berjangka</p>
								</div>
							</div>
							<hr>
							<div class="post-thumbnail">
								<div class="post-thumbnail-content">
									<span class="btn btn-primary btn-sm">01 Agustus 2021</span>
									<a href="#">
										<p><strong>Nomor 5 Tahun 2021</strong></p>
										<p style="color: #3c4043;">Tupoksi : Perdagangan Berjangka Komoditi</p>
									</a>
									<p>Tentang : Perubahan Ketiga Atas Peraturan Bappebti Nomor 1 Tahun 2019 Tentang Penetapan Daftar Bursa dan Kontrak Berjangka Luar Negeri Dalam Rangka Penyaluran Amanat Nasabah Ke Bursa Luar Negeri</p>
								</div>
							</div>
							<div class="post-thumbnail">
								<div class="post-thumbnail-content">
									<span class="btn btn-primary btn-sm">01 Mei 2021</span>
									<a href="#">
										<p><strong>Peraturan Bappebti</strong></p>
										<p style="color: #3c4043;">Tupoksi : Perdagangan Berjangka Komoditi</p>
									</a>
									<p>Tentang : PERUBAHAN KEDUA ATAS PERATURAN BADAN PENGAWAS PERDAGANGAN BERJANGKA KOMODITI NOMOR 99/BAPPEBTI/PER/11/2012 TENTANG PENERIMAAN NASABAH SECARA ELEKTRONIK ON-LINE DI BIDANG PERDAGANGAN BERJANGKA KOMODITI</p>
								</div>
							</div>
							<hr>
							<div class="post-thumbnail">
								<div class="post-thumbnail-content">
									<span class="btn btn-primary btn-sm">01 Agustus 2021</span>
									<a href="#">
										<p><strong>Nomor 5 Tahun 2021</strong></p>
										<p style="color: #3c4043;">Tupoksi : Perdagangan Berjangka Komoditi</p>
									</a>
									<p>Tentang : Tata Cara Kriteria Penetapan Daftar Orang Dalam Catatan Dan Daftar Orang Dalam Pemantauan Di Bidang Perdagangan Berjangka Komoditi</p>
								</div>
							</div>
							<hr>
							<div class="post-thumbnail">
								<button class="btn btn-outline-primary">LIHAT SEMUA</button>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- SIDEBAR RIGHT-->
			<div class="sidebar col-lg-4">
				<!--Tabs with Posts-->
				<div class="widget ">
					<div class="post-thumbnail-list">
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<span type="button" class="btn btn-primary btn-sm">SK/ Kep. Kepala Bappebti Terpopuler</span>
								<br><br>
								<a href="#">Nomor 4 Tahun 2018</a>
								<p>kskdsdksdksjn ds kdsj skdsdjksk ksd</p>
								<span class="post-date">01 Agustus 2018</span>
								<span class="post-category">Views : 6817</span>
							</div>
						</div>
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<a href="#">Nomor 5 Tahun 2019</a>
								<span class="post-date">01 Februari 2019</span>
								<span class="post-category">Views : 5870</span>
							</div>
						</div>
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<a href="#">Nomor 5 Tahun 2017</a>
								<span class="post-date">30 Mei 2017</span>
								<span class="post-category">Views : 5642</span>
							</div>
						</div>
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<a href="#">Nomor 8 Tahun 2019</a>
								<span class="post-date">01 Juli 2019</span>
								<span class="post-category">Views : 5551</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- end: SIDEBAR RIGHT-->
		</div>
	</div>
</section>
<!-- end: KEPUTUSAN KEPALA BAPPEBTI -->

<!-- FOOTER -->
<?php $this->load->view('include/footer'); ?>
<!-- end: FOOTER -->
