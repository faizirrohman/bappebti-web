<!-- HEADER -->
<?php $this->load->view('include/header'); ?>
<!-- end: HEADER -->

<section class="sidebar-right background-grey">
	<div class="container">
		<div class="heading-text">
			<!-- BREADCRUMB -->
			<div class="breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html">Halaman Utama</a></li>
						<li class="breadcrumb-item"><a href="#">PBK</a></li>
						<li class="breadcrumb-item active" aria-current="page">Edaran Kepala Bappebti</li>
					</ol>
				</nav>
			</div>
			<!-- end: BREADCRUMB -->
			<hr>
			<h4>Edaran Kepala Bappebti</h4>
		</div>
		<div class="row">
			<div class="content col-lg-8">
				<form action="" method="post">
					<div class="row">
						<div class="col-lg-5">
							<input type="text" name="" class="form-control background-grey" placeholder="Judul">
						</div>
						<div class="col-lg-3">
							<select name="" id="" class="form-control background-grey">
								<option selected>Bulan</option>
							</select>
						</div>
						<div class="col-lg-2">
							<select name="" id="" class="form-control background-grey">
								<option selected>Tahun</option>
							</select>
						</div>
						<div class="col-lg-2">
							<button type="submit" id="btn-search" class="form-control" style="background-color: #6cb6d8;"><i style="color: white;" class="icon-search"></i></button>
						</div>
					</div>
				</form>
				<br>
				<div id="blog">
					<div class="post-item">
						<div class="post-thumbnail-list">
							<div class="post-thumbnail">
								<div class="post-thumbnail-content">
									<span class="btn btn-primary btn-sm">01 September 2021</span>
									<a href="<?php echo base_url('pbk/edaran_kepala_bappebti/detail') ?>">
										<p><strong>SURAT EDARAN KEPALA BAPPEBTI NOMOR 285/BAPPEBTI/SE/08/2021</strong></p>
									</a>
								</div>
							</div>
							<hr>
							<div class="post-thumbnail">
								<div class="post-thumbnail-content">
									<span class="btn btn-primary btn-sm">01 Agustus 2021</span>
									<a href="#">
										<p><strong>SURAT EDARAN NOMOR 276/BAPPEBTI/SE/07/2021</strong></p>
									</a>
								</div>
							</div>
							<hr>
							<div class="post-thumbnail">
								<div class="post-thumbnail-content">
									<span class="btn btn-primary btn-sm">01 April 2020</span>
									<a href="#">
										<p><strong>SURAT EDARAN KEPALA BAPPEBTI NOMOR 77/BAPPEBTI/SE/04/2020</strong></p>
									</a>
								</div>
							</div>
							<hr>
							<div class="post-thumbnail">
								<div class="post-thumbnail-content">
									<span class="btn btn-primary btn-sm">01 Januari 2020</span>
									<a href="#">
										<p><strong>Surat Edaran Nomor 758/BAPPEBTI/SE/12/2019</strong></p>
									</a>
								</div>
							</div>
							<hr>
							<div class="post-thumbnail">
								<div class="post-thumbnail-content">
									<span class="btn btn-primary btn-sm">01 Desember 2019</span>
									<a href="#">
										<p><strong>Surat Edaran Nomor 745/BAPPEBTI/SE/12/2019</strong></p>
									</a>
								</div>
							</div>
							<hr>
							<div class="post-thumbnail">
								<button class="btn btn-outline-primary">LIHAT SEMUA</button>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- SIDEBAR RIGHT-->
			<div class="sidebar col-lg-4">
				<div class="widget ">
					<div class="post-thumbnail-list">
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<span type="button" class="btn btn-primary btn-sm">Edaran Kepala Bappebti Terpopuler</span>
								<br><br>
								<a href="#">SURAT EDARAN KEPALA BAPPEBTI NOMOR 77/BAPPEBTI/SE/04/2020</a>
								<span class="post-date">01 April 2020</span>
								<span class="post-category">Views : 2397</span>
							</div>
						</div>
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<a href="#">Surat Edaran Nomor : 76/BAPPEBTI/SE/03/2018</a>
								<span class="post-date">01 Maret 2018</span>
								<span class="post-category">Views : 1728</span>
							</div>
						</div>
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<a href="#">Surat Edaran Nomor 51/BAPPEBTI/SE/03/2017</a>
								<span class="post-date">14 Maret 2017</span>
								<span class="post-category">Views : 1449</span>
							</div>
						</div>
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<a href="#">Surat Edaran Nomor 758/BAPPEBTI/SE/12/2019</a>
								<span class="post-date">01 Januari 2020</span>
								<span class="post-category">Views : 1446</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- end: SIDEBAR RIGHT-->
		</div>
	</div>
</section>

<!-- FOOTER -->
<?php $this->load->view('include/footer'); ?>
<!-- end: FOOTER -->
