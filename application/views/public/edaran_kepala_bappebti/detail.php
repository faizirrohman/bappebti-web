<!-- HEADER -->
<?php $this->load->view('include/header'); ?>
<!-- end: HEADER -->

<section class="sidebar-right background-grey">
	<div class="container">
		<div class="heading-text">
			<!-- BREADCRUMB -->
			<div class="breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html">Halaman Utama</a></li>
						<li class="breadcrumb-item"><a href="#">PBK</a></li>
						<li class="breadcrumb-item"><a href="pbk-kep-kepala-bappebti.html">Edaran Kepala Bappebti</a></li>
						<li class="breadcrumb-item active" aria-current="page">Detail Edaran Kepala Bappebti</li>
					</ol>
				</nav>
			</div>
			<!-- end: BREADCRUMB -->
		</div>
		<div class="row">
			<div class="content col-lg-8">
				<div id="blog" class="grid-layout post-1-columns m-b-30" data-item="post-item">
					<div class="post-item">
						<div style="border: 1px solid rgb(205, 205, 205);">
							<div class="post-item-description" style="padding: 35px;">
								<h2>Surat Edaran Kepala Bappebti Nomor 285/bappebti/se/08/2021</h2>
								<hr><br>
								<iframe src='<?php echo base_url('assets/pdf/pbk/edaran_kepala_bappebti.pdf') ?>' width="650" height="490"></iframe>
								<br>
								<p>Jika Load PDF Gagal. Klik Link Ini untuk <a href="'<?php echo base_url('assets/pdf/pbk/edaran_kepala_bappebti.pdf') ?>" target="blank"><strong style="color: black;">Download PDF </strong></a></p>
								<br><hr><br>
								<div class="col-12">
									<div class="row">
										<div class="col-6">
											<strong style="color: black;">AUTHOR :</strong> <span style="color: blue;">Rorundak</span>
										</div>
										<div class="col-6">
											<strong style="color: black;">TAGS</strong>
										</div>
									</div>
									<div class="row">
										<div class="col-6">
											<span>PENYAMPAIAN LAPORAN BERKALA DAN SEWAKTU-WAKTU ATAS PELAKSANAAN PERDAGANGAN ASET KRIPTO</span>
										</div>
										<div class="col-6">
											<button style="color: grey; border: 0px">Edaran Kepala Bappebti</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- SIDEBAR RIGHT-->
			<div class="sidebar col-lg-4">
				<div class="widget">
					<div class="post-thumbnail-list">
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<span type="button" class="btn btn-primary btn-sm">SK/ Kep. Kepala Bappebti Terpopuler</span>
								<br><br>
								<a href="#">SURAT EDARAN KEPALA BAPPEBTI NOMOR 77/BAPPEBTI/SE/04/2020</a>
								<span class="post-date">01 April 2020</span>
								<span class="post-category">Views : 2397</span>
							</div>
						</div>
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<a href="#">Surat Edaran Nomor : 76/BAPPEBTI/SE/03/2018</a>
								<span class="post-date">01 Maret 2018</span>
								<span class="post-category">Views : 1728</span>
							</div>
						</div>
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<a href="#">Surat Edaran Nomor 51/BAPPEBTI/SE/03/2017</a>
								<span class="post-date">14 Maret 2017</span>
								<span class="post-category">Views : 1449</span>
							</div>
						</div>
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<a href="#">Surat Edaran Nomor 758/BAPPEBTI/SE/12/2019</a>
								<span class="post-date">01 Januari 2020</span>
								<span class="post-category">Views : 1446</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- end: SIDEBAR RIGHT-->
		</div>
	</div>
</section>

<!-- FOOTER -->
<?php $this->load->view('include/footer'); ?>
<!-- end: FOOTER -->
