<div class="container">
	<div class="row ">
		<div class="col-lg-6">
			<div class="heading-text">
				<h4>Siaran Pers</h4>
			</div>
		</div>
		<div class="col-lg-6" style="text-align: right;">
			<div class="heading-text">
				<a href="<?php echo base_url('siaran_pers') ?>"><strong style="color: #589bba !important; font-size: 15px;">SEMUA SIARAN PERS <i class="fa fa-angle-right "></i></strong></a>
			</div>
		</div>
	</div>
</div>
<div id="blog" class="grid-layout post-4-columns m-b-30" data-item="post-item">
	<div class="post-item">
		<div class="post-item-wrap">
			<div class="post-image">
				<a href="<?php echo base_url('siaran_pers/detail') ?>">
					<img alt="" src="<?php echo base_url('assets/images/siaran-pers/2.png') ?>">
				</a>
			</div>
			<div class="post-item-description">
				<div>
					<span type="button" class="btn btn-primary btn-sm">Siaran Pers</span>
				</div>
				<h2>
					<a href="<?php echo base_url('siaran_pers/detail') ?>">Pencabutan Izin Usaha Untuk Menyelenggarakan Kegiatan Sebag...</a>
				</h2>
				<span class="post-meta-date"><i class="fa fa-calendar-o"></i>28 Mei 2021</span>
			</div>
		</div>
	</div>
	<div class="post-item">
		<div class="post-item-wrap">
			<div class="post-image">
				<a href="<?php echo base_url('siaran_pers/detail') ?>">
					<img alt="" src="<?php echo base_url('assets/images/siaran-pers/3.png') ?>">
				</a>
			</div>
			<div class="post-item-description">
				<div>
					<span type="button" class="btn btn-primary btn-sm">Siaran Pers</span>
				</div>
				<h2>
					<a href="<?php echo base_url('siaran_pers/detail') ?>">Pencabutan Izin Usaha Untuk Menyelenggarakan Kegiatan Sebag...</a>
				</h2>
				
				<span class="post-meta-date"><i class="fa fa-calendar-o"></i>28 Mei 2021</span>
			</div>
		</div>
	</div>
	<div class="post-item">
		<div class="post-item-wrap">
			<div class="post-image">
				<a href="<?php echo base_url('siaran_pers/detail') ?>">
					<img alt="" src="<?php echo base_url('assets/images/siaran-pers/4.png') ?>">
				</a>
			</div>
			<div class="post-item-description">
				<div>
					<span type="button" class="btn btn-primary btn-sm">Siaran Pers</span>
				</div>
				<h2>
					<a href="<?php echo base_url('siaran_pers/detail') ?>">Pencabutan Izin Usaha Untuk Menyelenggarakan Kegiatan Sebag...</a>
				</h2>
				<span class="post-meta-date"><i class="fa fa-calendar-o"></i>28 Mei 2021</span>
			</div>
		</div>
	</div>
	<div class="post-item">
		<div class="post-item-wrap">
			<div class="post-image">
				<a href="<?php echo base_url('siaran_pers/detail') ?>">
					<img alt="" src="<?php echo base_url('assets/images/siaran-pers/5.png') ?>">
				</a>
			</div>
			<div class="post-item-description">
				<div>
					<span type="button" class="btn btn-primary btn-sm">Siaran Pers</span>
				</div>
				<h2>
					<a href="<?php echo base_url('siaran_pers/detail') ?>">Pencabutan Izin Usaha Untuk Menyelenggarakan Kegiatan Sebag...</a>
				</h2>

				<span class="post-meta-date"><i class="fa fa-calendar-o"></i>28 Mei 2021</span>
			</div>
		</div>
	</div>
</div>
