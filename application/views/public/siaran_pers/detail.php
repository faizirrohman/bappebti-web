<!-- HEADER -->
<?php $this->load->view('include/header'); ?>
<!-- end: HEADER -->

<!-- DETAIL SIARAN PERS -->
<section class="sidebar-right background-grey">
	<div class="container">
		<!-- BREADCRUMB -->
		<div class="breadcrumb">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo base_url('home') ?>">Halaman Utama</a></li>
					<li class="breadcrumb-item"><a href="<?php echo base_url('berita/siaran-pers') ?>">Siaran Pers</a></li>
					<li class="breadcrumb-item active" aria-current="page">Detail Siaran Pers</li>
				</ol>
			</nav>
		</div>
		<!-- end: BREADCRUMB -->
		<hr>
		<div class="row">
			<div class="content col-lg-8">
				<div class="heading-text">
					<button class="btn btn-primary btn-sm">SIARAN PERS</button>
					<h4 style="font-size: 25px;">Pencabutan Izin Usaha Untuk Menyelenggarakan Kegiatan Sebagai Pialang Berjangka Atas Nama PT Pruton Mega Berjangka</h4>
				</div>
				<div id="blog">
					<div class="post-item">
						<div class="post-image">
							<a href="#">
								<img alt="" src="<?php echo base_url('assets/images/siaran-pers/1.png') ?>">
							</a>
						</div>
						<section id="page-content" class="sidebar-left background-grey">
							<div class="container">
								<div class="row">
									<div class="content col-lg-10">
										<div id="blog">
											<div class="post-item">
												<div class="post-image">
													<div class="post-thumbnail-content">
														<p>Best practices. Accountable talk mumbo jumbo or hit the ground running. After Iran into H elen at a restaurant, I realized she was just o ff ice pretty this vendor
															is incompetent closer to the metal but viral engagement.</p>
														<p>We need to harvest synergy e ff ects put a record on and see who dances, nor not enough bandwidth. L ooks great, can we try it a di ff erent way. Killing it action item price point, or low - hanging f ruit, and we are running out o f runway
															f or eat our own dog f ood accountable talk.</p>
														<p>Finance action item, and bells and whistles, f or q uick - win, but bleeding edge, and eat our own dog f ood yet gain traction. In an ideal world both the angel on
															my le f t shoulder and the devil on my right are eager to go to the next board meeting and say we ’ re ditching the business model commitment to the cause
															looks great, can we try it a di ff erent way, so big picture five - year strategic plan.</p>
														<p>Nail it down back to the drawing - board out o f the loop. Blue sky thinking today shall be a cloudy day, thanks to blue sky thinking, we can now deploy our new ui
															to the cloud f eed the algorithm that ' s not on the roadmap but let ' s prioritize the low - hanging f ruit yet nail jelly to the hothouse wall, and thinking outside thebox.</p>
														<p>Who ' s the goto on this job with the way f orward we need a paradigm shi f t. O pen door policy proceduralize, so c - suite or idea shower organic growth, and we
															want to see more charts. All hands on deck back - end o f third q uarter. C annibalize looks great, can we try it a di ff erent way, yet ultimate measure of success.</p>
														<br>
														<button class="btn btn-outline-primary">Download PDF</button>
														<button class="btn btn-outline-primary">Komentar</button>
													</div>
												</div>
											</div>
										</div>
									</div>

									<!-- SIDEBAR LEFT-->
									<div class="sidebar col-lg-2">
										<div class="widget" style="text-align: center;">
											<div class="post-thumbnail-list">
												<div class="post-thumbnail">
													<img alt="" style="width: 80px; height: 80px;" src="<?php echo base_url('assets/images/logo-pp/1.png') ?>">
												</div><br>
												<strong>Humas</strong>
											</div>
											<div class="social-icons m-t-30 social-icons-colored">
												<li class="social-facebook">
													<a href="http://www.facebook.com/bappebti.kementerianperdagangan" target="blank"><i class="fab fa-facebook-f"></i></a>
												</li>
												<li class="social-twitter">
													<a href="http://twitter.com/InfoBappebti" target="blank"><i class="fab fa-twitter"></i></a>
												</li>
												<li class="social-twitter">
													<a href="http://www.instagram.com/bappebti/" target="blank"><i class="fab fa-instagram"></i></a>
												</li>
												<li class="social-twitter">
													<a href="#" target="blank"><i class="fab fa-youtube"></i></a>
												</li>
											</div>
										</div>
									</div>
									<!-- end: SIDEBAR LEFT-->
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>

			<!-- SIDEBAR RIGHT-->
			<div class="sidebar col-lg-4">
				<div class="widget ">
					<div class="post-thumbnail-list">
						<div class="post-thumbnail-entry">
							<div class="post-image">
								<a href="#">
									<img alt="" style="height: 180px;" src="<?php echo base_url('assets/images/siaran-pers/2.png') ?>">
								</a>
							</div>
							<div class="post-thumbnail-content">
								<span type="button" class="btn btn-primary btn-sm">SIARAN PERS TERPOPULER</span>
								<br><br>
								<a href="#">Bappebti Kembali Blokir 137 Entitas Tak Berizin, Termasuk Penawaran Investasi Forex Melalui P...</a>
								<span class="post-date">21 Mei 2021</span>
							</div>
						</div>
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<a href="#">Bappebti Kembali Blokir 137 Entitas Tak Berizin, Termasuk Penawaran Investasi Forex Melalui P...</a>
								<span class="post-date">19 Mei 2021</span>
							</div>
						</div>
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<a href="#">Waspada, Edccash Penipuan Investasi Berkedok Aset Kripto Dengan Skema Piramida</a>
								<span class="post-date">30 April 2021</span>
								<br><br>
								<span type="button" class="btn btn-primary btn-sm">SIARAN PERS TERBARU</span>
								<br><br>
								<a href="#">Pencabutan Izin Usaha Untuk Menyelenggarakan Kegiatan Sebagai Pialang Berjangka Atas Nam...</a>
								<span class="post-date">28 Mei 2021</span>
							</div>
						</div>
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<a href="#">Bappebti Kembali Blokir 137 Entitas Tak Berizin, Termasuk Penawaran Investasi Forex Melalui P...</a>
								<span class="post-date">19 Mei 2021</span>
							</div>
						</div>
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<a href="#">Waspada, Edccash Penipuan Investasi Berkedok Aset Kripto Dengan Skema Piramida</a>
								<span class="post-date">30 April 2021</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- end: SIDEBAR RIGHT-->
		</div>
	</div>
</section>
<!-- end: DETAIL SIARAN PERS -->

<!-- BERITA TERKAIT -->
<section id="page-content">
	<div id="blog">
		<div class="container">
			<div id="blog">
				<div class="container">
					<div class="row ">
						<div class="col-lg-6">
							<div class="heading-text">
								<h4>Berita Terkait</h4>
							</div>
						</div>
					</div>
				</div>
				<div id="blog" class="grid-layout post-4-columns m-b-30" data-item="post-item">
					<div class="post-item">
						<div class="post-item-wrap">
							<div class="post-image">
								<a href="#">
									<img alt="" src="<?php echo base_url('assets/images/siaran-pers/2.png') ?>">
								</a>
							</div>
							<div class="post-item-description">
								<div>
									<span type="button" class="btn btn-primary btn-sm">Siaran Pers</span>
								</div>
								<h2>
									<a href="detail-siaran-pers.html">Pencabutan Izin Usaha Untuk Menyelenggarakan Kegiatan Sebag...</a>
								</h2>
								<span class="post-meta-date"><i class="fa fa-calendar-o"></i>28 Mei 2021</span>
							</div>
						</div>
					</div>
					<div class="post-item">
						<div class="post-item-wrap">
							<div class="post-image">
								<a href="#">
									<img alt="" src="<?php echo base_url('assets/images/siaran-pers/3.png') ?>">
								</a>
							</div>
							<div class="post-item-description">
								<div>
									<span type="button" class="btn btn-primary btn-sm">Siaran Pers</span>
								</div>
								<h2>
									<a href="detail-siaran-pers.html">Pencabutan Izin Usaha Untuk Menyelenggarakan Kegiatan Sebag...</a>
								</h2>
								
								<span class="post-meta-date"><i class="fa fa-calendar-o"></i>28 Mei 2021</span>
							</div>
						</div>
					</div>
					<div class="post-item">
						<div class="post-item-wrap">
							<div class="post-image">
								<a href="#">
									<img alt="" src="<?php echo base_url('assets/images/siaran-pers/4.png') ?>">
								</a>
							</div>
							<div class="post-item-description">
								<div>
									<span type="button" class="btn btn-primary btn-sm">Siaran Pers</span>
								</div>
								<h2>
									<a href="detail-siaran-pers.html">Pencabutan Izin Usaha Untuk Menyelenggarakan Kegiatan Sebag...</a>
								</h2>
								<span class="post-meta-date"><i class="fa fa-calendar-o"></i>28 Mei 2021</span>
							</div>
						</div>
					</div>
					<div class="post-item">
						<div class="post-item-wrap">
							<div class="post-image">
								<a href="#">
									<img alt="" src="<?php echo base_url('assets/images/siaran-pers/5.png') ?>">
								</a>
							</div>
							<div class="post-item-description">
								<div>
									<span type="button" class="btn btn-primary btn-sm">Siaran Pers</span>
								</div>
								<h2>
									<a href="detail-siaran-pers.html">Pencabutan Izin Usaha Untuk Menyelenggarakan Kegiatan Sebag...</a>
								</h2>

								<span class="post-meta-date"><i class="fa fa-calendar-o"></i>28 Mei 2021</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- end: BERITA TERKAIT -->

<!-- FOOTER -->
<?php $this->load->view('include/footer'); ?>
<!-- end: FOOTER -->
