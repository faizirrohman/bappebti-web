<!-- HEADER -->
<?php $this->load->view('include/header'); ?>
<!-- end: HEADER -->

<!-- SIARAN PERS -->
<section class="sidebar-right background-grey">
	<div class="container">
		<div class="heading-text">
			<!-- BREADCRUMB -->
			<div class="breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?php echo base_url('home') ?>">Halaman Utama</a></li>
						<li class="breadcrumb-item active" aria-current="page">Siaran Pers</li>
					</ol>
				</nav>
			</div>
			<!-- end: BREADCRUMB -->
			<hr>
			<h4>Siaran Pers</h4>
		</div>
		<div class="row">
			<div class="content col-lg-8">
				<div id="blog">
					<div class="post-item">
						<div class="post-image">
							<a href="#">
								<img alt="" src="<?php echo base_url('assets/images/siaran-pers/1.png') ?>">
							</a>
						</div>
					</div>
					<div class="grid-layout post-2-columns m-b-30" data-item="post-item">
						<div class="post-item">
							<div class="post-item-wrap">
								<div class="post-image">
									<a href="<?php echo base_url('siaran_pers/detail') ?>">
										<img alt="" src="<?php echo base_url('assets/images/siaran-pers/2.png') ?>">
									</a>
								</div>
								<div class="post-item-description" style="margin: 12px;">
									<div>
										<span type="button" class="btn btn-primary btn-sm">Siaran Pers</span>
									</div>
									<h2>
										<a href="<?php echo base_url('siaran_pers/detail') ?>">Bappebti Kembali Blokir 137 Entitas Tak Berizin, Termasuk Penawara...</a>
									</h2>
									<span class="post-meta-date"><i class="fa fa-calendar-o"></i>28 Mei 2021</span>
								</div>
							</div>
						</div>
						<div class="post-item">
							<div class="post-item-wrap">
								<div class="post-image">
									<a href="<?php echo base_url('siaran_pers/detail') ?>">
										<img alt="" src="<?php echo base_url('assets/images/siaran-pers/3.png') ?>">
									</a>
								</div>
								<div class="post-item-description" style="margin: 12px;">
									<div>
										<span type="button" class="btn btn-primary btn-sm">Siaran Pers</span>
									</div>
									<h2>
										<a href="<?php echo base_url('siaran_pers/detail') ?>">Waspada, Edccash Penipuan Investasi Berkedok Aset Kripto Dengan Skem...</a>
									</h2>
									
									<span class="post-meta-date"><i class="fa fa-calendar-o"></i>28 Mei 2021</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="blog" class="post-thumbnails">
					<div class="post-item">
						<div class="post-image">
							<a href="#">
								<img alt="" style="height: 180px;" src="<?php echo base_url('assets/images/siaran-pers/4.png') ?>">
							</a>
						</div>
						<div class="post-item-description">
							<div class="post-thumbnail-list">
								<div class="post-thumbnail-entry">
									<div class="post-thumbnail-content" style="margin: 10px;">
										<span type="button" class="btn btn-primary btn-sm">Siaran Pers</span>
										<br>
										<a href="#">Pencabutan Izin Usaha Untuk Menyelenggarakan Kegiatan Sebagai Pialang Berjangka Atas Nam...</a>
										<span class="post-meta-date"></i>Humas - 28 Mei 2021</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="post-item">
						<div class="post-image">
							<a href="#">
								<img alt="" style="height: 180px;" src="<?php echo base_url('assets/images/siaran-pers/5.png') ?>">
							</a>
						</div>
						<div class="post-item-description">
							<div class="post-thumbnail-list">
								<div class="post-thumbnail-entry">
									<div class="post-thumbnail-content" style="margin: 10px;">
										<span type="button" class="btn btn-primary btn-sm">Siaran Pers</span>
										<br>
										<a href="#">Bertemu Bupati Batubara, Wamendag Dorong Implementasi Srg untuk Tingkatkan Kesejahteraan Petani</a>
										<span class="post-meta-date"></i>Humas - 28 Mei 2021</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="post-item">
						<div class="post-image">
							<a href="#">
								<img alt="" style="height: 180px;" src="<?php echo base_url('assets/images/siaran-pers/2.png') ?>">
							</a>
						</div>
						<div class="post-item-description">
							<div class="post-thumbnail-list">
								<div class="post-thumbnail-entry">
									<div class="post-thumbnail-content" style="margin: 10px;">
										<span type="button" class="btn btn-primary btn-sm">Siaran Pers</span>
										<br>
										<a href="#">Dorong Sosialisasi Dan Edukasi Perdagangan Berjangka Kopi, Kemendag Dukung Kolaborasi Jfx Dan Aeki</a>
										<span class="post-meta-date"></i>Humas - 28 Mei 2021</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="post-item">
						<div class="post-image">
							<a href="#">
								<img alt="" style="height: 180px;" src="<?php echo base_url('assets/images/siaran-pers/3.png') ?>">
							</a>
						</div>
						<div class="post-item-description">
							<div class="post-thumbnail-list">
								<div class="post-thumbnail-entry">
									<div class="post-thumbnail-content" style="margin: 10px;">
										<span type="button" class="btn btn-primary btn-sm">Siaran Pers</span>
										<br>
										<a href="#">Optimalkan Peran Pbk, bappebti Kemendag Gencarkan Edukasi Ke Kampus</a>
										<span class="post-meta-date"></i>Humas - 28 Mei 2021</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="post-item">
						<div class="post-image">
							<a href="#">
								<img alt="" style="height: 180px;" src="<?php echo base_url('assets/images/siaran-pers/4.png') ?>	">
							</a>
						</div>
						<div class="post-item-description">
							<div class="post-thumbnail-list">
								<div class="post-thumbnail-entry">
									<div class="post-thumbnail-content" style="margin: 10px;">
										<span type="button" class="btn btn-primary btn-sm">Siaran Pers</span>
										<br>
										<a href="#">Gerakkan Perekonomian Nasional Dan Tingkatkan Kesejahteraan, Pemerintah Dorong Penggunaan Srg</a>
										<span class="post-meta-date"></i>Humas - 28 Mei 2021</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- SIDEBAR RIGHT-->
			<div class="sidebar col-lg-4">
				<!--Tabs with Posts-->
				<div class="widget ">
					<div class="post-thumbnail-list">
						<div class="post-thumbnail-entry">
							<div class="post-image">
								<a href="#">
									<img alt="" style="height: 180px;" src="<?php echo base_url('assets/images/siaran-pers/1.png') ?>">
								</a>
							</div>
							<div class="post-thumbnail-content">
								<span type="button" class="btn btn-primary btn-sm">SIARAN PERS TERPOPULER</span>
								<br><br>
								<a href="#">Pencabutan Izin Usaha Untuk Menyelenggarakan Kegiatan Sebagai Pialang Berjangka Atas Nam...</a>
								<span class="post-date">21 Mei 2021</span>
							</div>
						</div>
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<a href="#">Bappebti Kembali Blokir 137 Entitas Tak Berizin, Termasuk Penawaran Investasi Forex Melalui P...</a>
								<span class="post-date">19 Mei 2021</span>
							</div>
						</div>
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<a href="#">Waspada, Edccash Penipuan Investasi Berkedok Aset Kripto Dengan Skema Piramida</a>
								<span class="post-date">30 April 2021</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- end: SIDEBAR RIGHT-->
		</div>
	</div>
</section>
<!-- end: SIARAN PERS -->

<!-- FOOTER -->
<?php $this->load->view('include/footer'); ?>
<!-- end: FOOTER -->
