<!-- HEADER -->
<?php $this->load->view('include/header'); ?>
<!-- end: HEADER -->

<!-- DETAIL PERATURAN PEMERINTAH PBK -->
<section class="background-grey">
	<div class="container">
		<!-- BREADCRUMB -->
		<div class="breadcrumb">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo base_url('home') ?>">Halaman Utama</a></li>
					<li class="breadcrumb-item"><a href="#">PBK</a></li>
					<li class="breadcrumb-item"><a href="<?php echo base_url('pbk/peraturan-pemerintah') ?>">Peraturan Pemerintah</a></li>
					<li class="breadcrumb-item active" aria-current="page">Detail Peraturan Pemerintah</li>
				</ol>
			</nav>
		</div>
		<!-- end: BREADCRUMB -->
		<div id="blog" class="grid-layout post-1-columns m-b-30" data-item="post-item">
			<div class="post-item">
				<div class="post-item-wrap" >
					<div class="post-item-description" style="padding: 35px;">
						<h2>BAB I KETENTUAN UMUM</h2>
						<hr><br>
						<br><br><hr><br>
						<div class="col-12">
							<div class="row">
								<div class="col-6">
									<strong style="color: black;">AUTHOR :</strong> <span style="color: blue;">Admin</span>
								</div>
								<div class="col-6">
									<strong style="color: black;">TAGS</strong>
								</div>
							</div>
							<div class="row">
								<div class="col-6">
									<p>BAB I KETENTUAN UMUM</p>
								</div>
								<div class="col-6">
									<button style="color: grey; border: 0px">Peraturan Pemerintah</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- end: DETAIL PERATURAN PEMERINTAH PBK -->

<!-- FOOTER -->
<?php $this->load->view('include/footer'); ?>
<!-- end: FOOTER -->
