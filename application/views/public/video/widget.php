<div class="heading-text">
	<h4 style="color: white;">Tonton apa yang kami buat untuk Anda</h4>
</div>
<div class="row">
	<div class="content col-lg-9">
		<div class="post-item border">
			<div class="post-item-wrap" style="background-color: #103A56; border-radius: 9px; border: #103A56 !important;">
				<div class="post-video">
					<div class="ratio ratio-16x9">
						<iframe class="responsive-iframe rounded-top" width="560" height="376" src="https://www.youtube.com/embed/tgbNymZ7vqY" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
				<div class="post-item-description">
					<h2><a style="color: white;" href="#">Cuan Saham vs Aset Crypto, mana yang lebih menjanjikan ?</a></h2>
					<span class="post-meta-date"><i class="fa fa-calendar-o"></i>31 Mei 2021</span>
				</div>
			</div>
		</div>
	</div>

	<!-- SIDEBAR RIGHT-->
	<div class="sidebar col-lg-3" style="overflow: auto; height: 580px;">
		<div class="post-item border">
			<div class="post-item-wrap" style="background-color: #103A56; border-radius: 9px; border: #103A56 !important;">
				<div class="post-video">
					<div class="ratio ratio-16x9">
						<iframe class="responsive-iframe rounded-top" width="560" height="376" src="https://www.youtube.com/embed/tgbNymZ7vqY" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
				<div class="post-item-description">
					<h5><a href="#" style="color: white;">Creative Money, Jurus Investasi</a></h5>
					<br>
					<span class="post-meta-date"><i class="fa fa-calendar-o"></i>20 April 2021</span>
				</div>
			</div>
		</div>
		<div class="post-item border">
			<div class="post-item-wrap" style="background-color: #103A56; border-radius: 9px; border: #103A56 !important;">
				<div class="post-video">
					<div class="ratio ratio-16x9">
						<iframe class="responsive-iframe rounded-top" width="560" height="376" src="https://www.youtube.com/embed/tgbNymZ7vqY" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
				<div class="post-item-description">
					<h5><a href="#" style="color: white;">Investasi Aset Crypto: Ngeri atau Sedap?</a></h5>
					<br>
					<span class="post-meta-date"><i class="fa fa-calendar-o"></i>20 April 2021</span>
				</div>
			</div>
		</div>
		<div class="post-item border">
			<div class="post-item-wrap" style="background-color: #103A56; border-radius: 9px; border: #103A56 !important;">
				<div class="post-video">
					<div class="ratio ratio-16x9">
						<iframe class="responsive-iframe rounded-top" width="560" height="376" src="https://www.youtube.com/embed/tgbNymZ7vqY" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
				<div class="post-item-description">
					<h5><a href="#" style="color: white;">Investasi Aset Crypto Menggiurkan, Bagaimana P...</a></h5>
					<br>
					<span class="post-meta-date"><i class="fa fa-calendar-o"></i>20 April 2021</span>
				</div>
			</div>
		</div>
	</div>
	<!-- end: SIDEBAR RIGHT-->
</div>
