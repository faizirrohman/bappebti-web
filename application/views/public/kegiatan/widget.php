<div class="heading-text">
	<h4>Kegiatan</h4>
</div>
<div class="post-thumbnail-list" style="overflow: scroll; height: 300px; overflow-x: hidden;">
	<div class="post-thumbnail-entry">
		<div class="post-thumbnail-content">
			<a href="#">Bertemu Bupati Batubara, Wamendag Dorong Implementasi Srg untuk Tingkatkan Kesejahteraan Petani</a>
			<span class="post-date">28 Mei 2021</span>
		</div>
	</div>
	<div class="post-thumbnail-entry">
		<div class="post-thumbnail-content">
			<a href="#">Dukungan Rantai Bisnis Produk Perikanan, Kemendag Optimalkan Dan Perluas Pemanfaatan Srg</a>
			<span class="post-date">28 Mei 2021</span>
		</div>
	</div>
	<div class="post-thumbnail-entry">
		<div class="post-thumbnail-content">
			<a href="#">Gerakkan Perekonomian Nasional Dan Tingkatkan Kesejahteraan, Pemerintah Dorong Penggunaan Srg</a>
			<span class="post-date">28 Mei 2021</span>
		</div>
	</div>
</div>
<br>
<a href="<?php echo base_url('kegiatan') ?>" class="text-theme"><strong style="color: #589bba !important;">LIHAT SEMUA <i class="fa fa-angle-right "></strong></i></a>
<br><br>
