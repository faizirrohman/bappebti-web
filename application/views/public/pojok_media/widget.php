<div class="container">
	<div class="row ">
		<div class="col-lg-6">
			<div class="heading-text">
				<h4>Pojok Media</h4>
			</div>
		</div>
		<div class="col-lg-6" style="text-align: right;">
			<div class="heading-text">
				<a href="#"><strong style="color: #589bba !important; font-size: 15px;">SEMUA POJOK MEDIA <i class="fa fa-angle-right "></i></strong></a>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-6">
			<div class="post-item">
				<div class="post-item-wrap">
					<div class="post-image">
						<a href="#">
							<img alt="" src="<?php echo base_url('assets/images/pojok-media/5.png') ?>">
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<div id="blog" class="grid-layout post-2-columns m-b-30" data-item="post-item">
				<div class="post-item">
					<div class="post-item-wrap">
						<div class="post-image">
							<a href="#">
								<img alt="Responsive image" src="<?php echo base_url('assets/images/pojok-media/1.jpg') ?>">
							</a>
						</div>
						<div class="post-item-description">
							<div>
								<span type="button" class="btn btn-primary btn-sm">Pojok Media</span>
							</div>
							<h2><a href="#">Pengumuman Nama Peserta Yang Berhak Mengikuti Ujia...</a></h2>
							<span class="post-meta-date"><i class="fa fa-calendar-o"></i>28 Mei 2021</span>
						</div>
					</div>
				</div>
				<div class="post-item">
					<div class="post-item-wrap">
						<div class="post-image">
							<a href="#">
								<img alt="" src="<?php echo base_url('assets/images/pojok-media/2.jpg') ?>">
							</a>
						</div>
						<div class="post-item-description">
							<div>
								<span type="button" class="btn btn-primary btn-sm">Pojok Media</span>
							</div>
							<h2><a href="#">Pengumuman Nama Peserta Yang Berhak Mengikuti Ujia...</a></h2>
							<span class="post-meta-date"><i class="fa fa-calendar-o"></i>28 Mei 2021</span>
						</div>
					</div>
				</div>
				<div class="post-item">
					<div class="post-item-wrap">
						<div class="post-image">
							<a href="#">
								<img alt="" src="<?php echo base_url('assets/images/pojok-media/3.jpg') ?>">
							</a>
						</div>
						<div class="post-item-description">
							<div>
								<span type="button" class="btn btn-primary btn-sm">Pojok Media</span>
							</div>
							<h2><a href="#">Pengumuman Nama Peserta Yang Berhak Mengikuti Ujia...</a></h2>
							<span class="post-meta-date"><i class="fa fa-calendar-o"></i>28 Mei 2021</span>
						</div>
					</div>
				</div>
				<div class="post-item">
					<div class="post-item-wrap">
						<div class="post-image">
							<a href="#">
								<img alt="" src="<?php echo base_url('assets/images/pojok-media/4.jpg') ?>">
							</a>
						</div>
						<div class="post-item-description">
							<div>
								<span type="button" class="btn btn-primary btn-sm">Pojok Media</span>
							</div>
							<h2><a href="#">Pengumuman Nama Peserta Yang Berhak Mengikuti Ujia...</a></h2>
							<span class="post-meta-date"><i class="fa fa-calendar-o"></i>28 Mei 2021</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
