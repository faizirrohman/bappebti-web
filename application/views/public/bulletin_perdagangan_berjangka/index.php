<!-- HEADER -->
<?php $this->load->view('include/header'); ?>
<!-- end: HEADER -->

<section class="background-grey">
	<div class="container">
		<div class="heading-text">
			<div class="breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="#">Halaman Utama</a></li>
						<li class="breadcrumb-item"><a href="#">Info Bappebti</a></li>
						<li class="breadcrumb-item"><a href="#">Publikasi</a></li>
						<li class="breadcrumb-item active" aria-current="page">Bulletin Perdagangan Berjangka</li>
					</ol>
				</nav>
			</div>
			<hr>
			<h4>Bulletin Perdagangan Berjangka</h4>
		</div>
		<div class="col-12">
			<div class="post-item">
				<div class="post-item-wrap" style="border-radius: 10px;">
					<div class="post-image">
						<form action="" method="post">
							<div class="row" style="padding: 20px;">
								<div class="col-lg-4">
									<input type="text" name="" class="form-control background-grey" style="border: 0px;" placeholder="Kata Kunci">
								</div>
								<div class="col-lg-3 dropdown">
									<select name="" id="" class="form-control background-grey" style="border: 0px;">
										<option selected>Bulan</i></option>
										<option value="">Januari</option>
										<option value="">Februari</option>
										<option value="">Maret</option>
										<option value="">April</option>
										<option value="">Mei</option>
										<option value="">Juni</option>
										<option value="">Juli</option>
										<option value="">Agustus</option>
										<option value="">September</option>
										<option value="">Oktober</option>
										<option value="">November</option>
										<option value="">Desember</option>
									</select>
								</div>
								<div class="col-lg-2 dropdown">
									<select name="" id="" class="form-control background-grey" style="border: 0px;">
										<option selected>Tahun</i></option>
										<option value="">1997</option>
										<option value="">1999</option>
										<option value="">2000</option>
										<option value="">2011</option>
										<option value="">2014</option>
										<option value="">2017</option>
									</select>
								</div>
								<div class="col-lg-1"></div>
								<div class="col-lg-2">
									<button type="submit" id="btn-search" class="form-control" style="background-color: #6cb6d8;"><i style="color: white;" class="icon-search"></i></button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12">
			<div id="blog" class="grid-layout post-3-columns m-b-30" data-item="post-item">
				<div class="post-item">
					<div class="post-item-wrap" style="border-radius: 10px;">
						<div class="post-image">
							<a href="#">
								<img alt="" class="rounded-top" style="height: 400px;" src="<?php echo base_url('assets/images/publikasi/2.png') ?>">
							</a>
						</div>
						<div class="post-item-description" style="padding: 28px;">
							<p>Bulletin Perdagangan Berjangka 2021</p>
							<span class="post-meta-date">03 Januari 2021</span><br><br>
							<button class="btn btn-outline-primary" style="width: 290px;"><i class="fa fa-download"></i> Download</button>
						</div>
					</div>
				</div>
				<div class="post-item">
					<div class="post-item-wrap" style="border-radius: 10px;">
						<div class="post-image">
							<a href="#">
								<img alt="" class="rounded-top" style="height: 400px;" src="<?php echo base_url('assets/images/publikasi/2.png') ?>">
							</a>
						</div>
						<div class="post-item-description" style="padding: 28px;">
							<p>Bulletin Perdagangan Berjangka 2020</p>
							<span class="post-meta-date">05 Januari 2020</span><br><br>
							<button class="btn btn-outline-primary" style="width: 290px;"><i class="fa fa-download"></i> Download</button>
						</div>
					</div>
				</div>
				<div class="post-item">
					<div class="post-item-wrap" style="border-radius: 10px;">
						<div class="post-image">
							<a href="#">
								<img alt="" class="rounded-top" style="height: 400px;" src="<?php echo base_url('assets/images/publikasi/2.png') ?>">
							</a>
						</div>
						<div class="post-item-description" style="padding: 28px;">
							<p>Bulletin Perdagangan Berjangka 2021</p>
							<span class="post-meta-date">07 Januari 2019</span><br><br>
							<button class="btn btn-outline-primary" style="width: 290px;"><i class="fa fa-download"></i> Download</button>
						</div>
					</div>
				</div>
				<div class="post-item">
					<div class="post-item-wrap" style="border-radius: 10px;">
						<div class="post-image">
							<a href="#">
								<img alt="" class="rounded-top" style="height: 400px;" src="<?php echo base_url('assets/images/publikasi/2.png') ?>">
							</a>
						</div>
						<div class="post-item-description" style="padding: 28px;">
							<p>Bulletin Perdagangan Berjangka 2019</p>
							<span class="post-meta-date">03 Januari 2021</span><br><br>
							<button class="btn btn-outline-primary" style="width: 290px;"><i class="fa fa-download"></i> Download</button>
						</div>
					</div>
				</div>
				<div class="post-item">
					<div class="post-item-wrap" style="border-radius: 10px;">
						<div class="post-image">
							<a href="#">
								<img alt="" class="rounded-top" style="height: 400px;" src="<?php echo base_url('assets/images/publikasi/2.png') ?>">
							</a>
						</div>
						<div class="post-item-description" style="padding: 28px;">
							<p>Bulletin Perdagangan Berjangka 2019</p>
							<span class="post-meta-date">07 Januari 2019</span><br><br>
							<button class="btn btn-outline-primary" style="width: 290px;"><i class="fa fa-download"></i> Download</button>
						</div>
					</div>
				</div>
				<div class="post-item">
					<div class="post-item-wrap" style="border-radius: 10px;">
						<div class="post-image">
							<a href="#">
								<img alt="" class="rounded-top" style="height: 400px;" src="<?php echo base_url('assets/images/publikasi/2.png') ?>">
							</a>
						</div>
						<div class="post-item-description" style="padding: 28px;">
							<p>Bulletin Perdagangan Berjangka 2019</p>
							<span class="post-meta-date">07 Januari 2019</span><br><br>
							<button class="btn btn-outline-primary" style="width: 290px;"><i class="fa fa-download"></i> Download</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- FOOTER -->
<?php $this->load->view('include/footer'); ?>
<!-- end: FOOTER -->
