<!-- HEADER -->
<?php $this->load->view('include/header'); ?>
<!-- end: HEADER -->

<!-- BACKGROUND HUBUNGI KAMI -->
<section class="parallax text-light halfscreen" data-bg-parallax="<?php echo base_url('assets/images/bg-all.png') ?>">
	<div class="container">
		<div class="container-fullscreen">
			<div class="text-middle text-center text-end">
				<h1 class="text-large" data-animate="animate__fadeInDown">Hubungi Kami</h1>
			</div>
		</div>
	</div>
</section>
<!-- end: BACKGROUND HUBUNGI KAMI -->


<!-- MESSAGE -->
<section>
	<div class="container">
		<form action="" method="post">
			<div class="row">
				<strong style="text-align: center;">Simply fill out this form</strong>
				<br><br>
				<div class="col-lg-6">
					<div class="form-group">
						<textarea type="text" style="border-radius: 0px; border: 0px;" name="widget-contact-form-message" required rows="8" class="form-control background-grey" placeholder="Enter your Message"></textarea>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="row">
						<div class="form-group col-md-12">
							<input type="text" name="name" style="border-radius: 0px; border: 0px;" class="form-control background-grey" placeholder="Name" required>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-12">
							<input type="email" name="email" style="border-radius: 0px; border: 0px;" class="form-control background-grey" placeholder="Email" required>
						</div>
					</div>
					<div class="form-group col-md-12">
						<button class="btn btn-primary form-control" style="border-radius: 0px;" type="submit" id="form-submit">Kirim</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</section>
<!-- end: MESSAGE -->

<!-- KONTAK -->
<section class="background-grey">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<h3 class="text-uppercase">Kunjungi Kami</h3>
				<div class="m-t-30">
					<form class="widget-contact-form" data-success-message-delay="40000" novalidate action="" role="form" method="post">
						<div class="row">
							<div class="form-group col-md-12">
								<strong>Alamat</strong>
								<p>Gedung Bappebti Lt. 1 - 7, Jl. Kramat Raya No. 172 Jakarta 10430</p>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-12">
								<strong>Telepon</strong>
								<p>0811 1109 901</p>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-12">
								<strong>SMS Center</strong>
								<p>(021) 3192 4744</p>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-12">
								<strong>Fax</strong>
								<p>(021) 3192 3204</p>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-12">
								<strong>Email</strong>
								<a href="mailto:humas.bappebti@kemendag.go.id">
									<p>humas.bappebti@kemendag.go.id</p>
								</a>
							</div>  
						</div>
					</form>
				</div>
			</div>
			<div class="col-lg-6">
				<!-- Google Map -->
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.550081281795!2d106.84505751415462!3d-6.190904695518112!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f446aedbaabb%3A0x6117f3318adb6c8!2sCommodity%20Futures%20Trading%20Regulatory%20Agency%20(BAPPEBTI)!5e0!3m2!1sen!2sid!4v1636092923096!5m2!1sen!2sid" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
				<!-- end: Google Map -->
			</div>
		</div>
	</div>
</section>
<!-- end: KONTAK -->

<!-- FOOTER -->
<?php $this->load->view('include/footer'); ?>
<!-- end: FOOTER -->
