<!-- HEADER -->
<?php $this->load->view('include/header'); ?>
<!-- end: HEADER -->

<!-- DETAIL UNDANG-UNDANG PBK -->
<section class="background-grey">
	<div class="container">
		<!-- BREADCRUMB -->
		<div class="breadcrumb">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo base_url('home') ?>">Halaman Utama</a></li>
					<li class="breadcrumb-item"><a href="#">PBK</a></li>
					<li class="breadcrumb-item"><a href="<?php echo base_url('pbk/undang-undang') ?>">Undang Undang</a></li>
					<li class="breadcrumb-item active" aria-current="page">Detail Undang Undang</li>
				</ol>
			</nav>
		</div>
		<!-- end: BREADCRUMB -->
		<div id="blog" class="grid-layout post-1-columns m-b-30" data-item="post-item">
			<div class="post-item">
				<div class="post-item-wrap">
					<div class="post-item-description" style="padding: 35px;">
						<h2>UNDANG-UNDANG REPUBLIK INDONESIA NOMOR 32 TAHUN 1997</h2>
						<hr><br>
						<iframe src="<?php echo base_url('assets/pdf/pbk/undang_undang.pdf') ?>" width="100%" height="500px"></iframe>
						<p>Jika Load PDF Gagal. Klik Link Ini untuk <a href="<?php echo base_url('assets/pdf/pbk/undang_undang.pdf') ?>" target="blank"><strong style="color: black;">Download PDF </strong></a></p>
						<br><hr><br>
						<div class="col-12">
							<div class="row">
								<div class="col-6">
									<strong style="color: black;">AUTHOR :</strong> <span style="color: blue;">Admin</span>
								</div>
								<div class="col-6">
									<strong style="color: black;">TAGS</strong>
								</div>
							</div>
							<div class="row">
								<div class="col-6">
									<p>UNDANG-UNDANG REPUBLIK INDONESIA NOMOR 32 TAHUN 1997</p>
								</div>
								<div class="col-6">
									<button style="color: grey; border: 0px">Undang undang</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- end: DETAIL UNDANG-UNDANG PBK -->

<!-- FOOTER -->
<?php $this->load->view('include/footer'); ?>
<!-- end: FOOTER -->
