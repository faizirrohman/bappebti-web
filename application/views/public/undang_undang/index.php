<!-- HEADER -->
<?php $this->load->view('include/header'); ?>
<!-- end: HEADER -->

<!-- PBK UNDANG UNDANG -->
<section class="sidebar-right background-grey">
	<div class="container">
		<div class="heading-text">
			<!-- BREADCRUMB -->
			<div class="breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?php echo base_url('home') ?>">Halaman Utama</a></li>
						<li class="breadcrumb-item"><a href="#">PBK</a></li>
						<li class="breadcrumb-item active" aria-current="page">Undang Undang</li>
					</ol>
				</nav>
			</div>
			<!-- end: BREADCRUMB -->
			<hr>
			<h4>Undang undang</h4>
		</div>
		<div class="row">
			<div class="content col-lg-8">
				<form action="" method="post">
					<div class="row">
						<div class="col-lg-5">
							<input type="text" name="" class="form-control background-grey" placeholder="Kata Kunci">
						</div>
						<div class="col-lg-3 dropdown">
							<select name="" id="" class="form-control background-grey">
								<option selected>All</i></option>
								<option value="">Undang-Undang</option>
								<option value="">Peraturan Pemerintah</option>
								<option value="">Kepres</option>
								<option value="">Kep. Per. Menteri</option>
								<option value="">SK. Kep. Kepala Bappebti</option>
								<option value="">Edaran Kepala Bappebti</option>
							</select>
						</div>
						<div class="col-lg-2 dropdown">
							<select name="" id="" class="form-control background-grey">
								<option selected>All</i></option>
								<option value="">1997</option>
								<option value="">1999</option>
								<option value="">2000</option>
								<option value="">2011</option>
								<option value="">2014</option>
								<option value="">2017</option>
							</select>
						</div>
						<div class="col-lg-2">
							<button type="submit" id="btn-search" class="form-control" style="background-color: #6cb6d8;"><i style="color: white;" class="icon-search"></i></button>
						</div>
					</div>
				</form>
				<br>
				<div id="blog">
					<div class="post-item">
						<div class="post-thumbnail-list">
							<div class="post-thumbnail">
								<div class="post-thumbnail-content">
									<span class="btn btn-primary btn-sm">Undang-Undang Republik Indonesia Nomor 32 Tahun 1997</span>
									<a href="<?php echo base_url('pbk/undang_undang/detail') ?>">
										<p><strong>Tentang Perdagangan Berjangka Komoditi</strong></p>
									</a>
								</div>
							</div>
							<hr>
							<div class="post-thumbnail">
								<div class="post-thumbnail-content">
									<span class="btn btn-primary btn-sm">Undang-Undang Republik Indonesia Nomor 10 Tahun 2011</span>
									<a href="">
										<p><strong>Tentang Perubahan Atas Undang-Undang Nomor 32 Tahun 1997 Tentang Perdagangan Berjangka Komoditi</strong></p>
									</a>
								</div>
							</div>
							<hr>
							<div class="post-thumbnail">
								<button class="btn btn-outline-primary">LIHAT SEMUA</button>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- SIDEBAR RIGHT-->
			<div class="col-lg-4">
				<div class="widget ">
					<div class="post-thumbnail-list">
						<div class="post-thumbnail-entry">
							<div class="post-image">
								<a href="#">
									<img alt="" src="<?php echo base_url('assets/images/kegiatan/1.png') ?>">
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- end: SIDEBAR RIGHT-->
		</div>
	</div>
</section>
<!-- end: PBK UNDANG UNDANG -->

<!-- FOOTER -->
<?php $this->load->view('include/footer'); ?>
<!-- end: FOOTER -->
