<div class="content col-lg-12">
	<div class="heading-text">
		<h4>Kerja Sama Bappebti</h4>
	</div>
	<div class="carousel client-logos" data-items="5" data-dots="false"> 
		<div>
			<a href="http://www.aspebtindo.org/" target="blank">
				<img alt="" src="<?php echo base_url('assets/images/clients/client-1.png') ?>">
			</a>
		</div>
		<div>
			<a href="http://www.jfx.co.id/id" target="blank">
				<img alt="" src="<?php echo base_url('assets/images/clients/client-2.png') ?>">
			</a>
		</div>
		<div>
			<a href="http://www.icdx.co.id/" target="blank">
				<img alt="" src="<?php echo base_url('assets/images/clients/client-3.png') ?>">
			</a>
		</div>
		<div>
			<a href="http://www.ptkbi.com/" target="blank">
				<img alt="" src="<?php echo base_url('assets/images/clients/client-4.png') ?>">
			</a>
		</div>
		<div>
			<a href="http://www.ich.co.id/" target="blank">
				<img alt="" src="<?php echo base_url('assets/images/clients/client-5.jpg') ?>">
			</a>
		</div>
		<div>
			<a href="http://www.bakti-arb.org/" target="blank">
				<img alt="" src="<?php echo base_url('assets/images/clients/client-6.png') ?>">
			</a>
		</div>
		<div>
			<a href="http://www.kemendag.go.id/" target="blank">
				<img alt="" src="<?php echo base_url('assets/images/clients/client-7.png') ?>">
			</a>
		</div>
	</div>
</div>
