<!-- HEADER -->
<?php $this->load->view('include/header'); ?>
<!-- end: HEADER -->

<!-- KEPUTUSAN PERATURAN MENTERI -->
<section class="sidebar-right background-grey">
	<div class="container">
		<div class="heading-text">
			<!-- BREADCRUMB -->
			<div class="breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="#">Halaman Utama</a></li>
						<li class="breadcrumb-item"><a href="#">PBK</a></li>
						<li class="breadcrumb-item active" aria-current="page">Kep./ Per. Menteri</li>
					</ol>
				</nav>
			</div>
			<!-- end: BREADCRUMB -->
			<hr>
			<h4>Kep./ Per. Menteri</h4>
		</div>
		<div class="row">
			<div class="content col-lg-8">
				<form action="" method="post">
					<div class="row">
						<div class="col-lg-5">
							<input type="text" name="" class="form-control background-grey" placeholder="Judul..">
						</div>
						<div class="col-lg-3">
							<select name="" id="" class="form-control background-grey">
								<option selected>Bulan</option>
							</select>
						</div>
						<div class="col-lg-2">
							<select name="" id="" class="form-control background-grey">
								<option selected>Tahun</option>
							</select>
						</div>
						<div class="col-lg-2">
							<button type="submit" id="btn-search" class="form-control" style="background-color: #6cb6d8;"><i style="color: white;" class="icon-search"></i></button>
						</div>
					</div>
				</form>
				<br>
				<div id="blog">
					<div class="post-item">
						<div class="post-thumbnail-list">
							<div class="post-thumbnail">
								<div class="post-thumbnail-content">
									<span type="button" class="btn btn-primary btn-sm">01 Agustus 2018</span>
									<a href="<?php echo base_url('pbk/per_kep_menteri/detail') ?>">
										<p><strong>PERATURAN MENTERI PERDAGANGAN RI NOMOR 77 TAHUN 2018</strong></p>
									</a>
								</div>
							</div>
							<hr>
							<div class="post-thumbnail">
								<div class="post-thumbnail-content">
									<span type="button" class="btn btn-primary btn-sm">01 Agustus 2000</span>
									<a href="#">
										<p><strong>PERATURAN MENTERI PERDAGANGAN RI NOMOR 76 TAHUN 2018</strong></p>
									</a>
								</div>
							</div>
							<hr>
							<div class="post-thumbnail">
								<div class="post-thumbnail-content">
									<span type="button" class="btn btn-primary btn-sm">01 November 2017</span>
									<a href="#">
										<p><strong>PERATURAN MENTERI PERDAGANGAN RI NOMOR 73/M-DAG/PER/9/2017</strong></p>
									</a>
								</div>
							</div>
							<hr>
							<div class="post-thumbnail">
								<div class="post-thumbnail-content">
									<span type="button" class="btn btn-primary btn-sm">22 Juni 2017</span>
									<a href="#">
										<p><strong>PERATURAN MENTERI PERDAGANGAN REPUBLIK INDONESIA NOMOR 40/M-DAG/PER/6/2017</strong></p>
									</a>
								</div>
							</div>
							<hr>
							<div class="post-thumbnail">
								<div class="post-thumbnail-content">
									<span type="button" class="btn btn-primary btn-sm">17 Maret 2017</span>
									<a href="#">
										<p><strong>PERATURAN MENTERI PERDAGANGAN RI NOMOR 16/M-DAG/PER/3/2017</strong></p>
									</a>
								</div>
							</div>
							<hr>
							<div class="post-thumbnail">
								<button class="btn btn-outline-primary">LIHAT SEMUA</button>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- SIDEBAR RIGHT-->
			<div class="sidebar col-lg-4">
				<!--Tabs with Posts-->
				<div class="widget ">
					<div class="post-thumbnail-list">
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<span type="button" class="btn btn-primary btn-sm">Kep./ Per. Menteri Terpopuler</span>
								<br><br>
								<a href="#">Peraturan Menteri Perdagangan RI Nomor 77 Tahun 2018</a>
								<span class="post-date">01 Agustus 2018</span>
								<span class="post-category">Views : 1955</span>
							</div>
						</div>
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<a href="#">Peraturan Menteri Perindustrian dan Perdagangan No 86/MPP/KEP/3/2001</a>
								<span class="post-date">04 Maret 2001</span>
								<span class="post-category">Views : 1737</span>
							</div>
						</div>
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<a href="#">Peraturan Menteri Perdagangan RI Nomor 73/M-DAG/PER/9/2017</a>
								<span class="post-date">01 November 2017</span>
								<span class="post-category">Views : 1463</span>
							</div>
						</div>
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<a href="#">Peraturan Menteri Perdagangan RI Nomor 01/M-DAG/PER/3/2005</a>
								<span class="post-date">04 April 2005</span>
								<span class="post-category">Views : 1399</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- end: SIDEBAR RIGHT-->
		</div>
	</div>
</section>
<!-- end: KEPUTUSAN PERATURAN MENTERI -->

<!-- FOOTER -->
<?php $this->load->view('include/footer'); ?>
<!-- end: FOOTER -->
