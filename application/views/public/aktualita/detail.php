<!-- HEADER -->
<?php $this->load->view('include/header'); ?>
<!-- end: HEADER -->

<section class="sidebar-right background-grey">
	<div class="container">
		<div class="heading-text">
			<!-- BREADCRUMB -->
			<div class="breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html">Halaman Utama</a></li>
						<li class="breadcrumb-item active" aria-current="page">Detail Aktualita</li>
					</ol>
				</nav>
			</div>
			<!-- end: BREADCRUMB -->
		</div>
		<div class="row">
			<div class="content col-lg-8">
				<div id="blog" class="grid-layout post-1-columns m-b-30" data-item="post-item">
					<div class="post-item">
						<div style="border: 1px solid rgb(205, 205, 205);">
							<div class="post-item-description" style="padding: 35px;">
								<h2>Peraturan Menteri Perdagangan Ri Nomor 77 Tahun 2018</h2>
								<hr><br>
									<iframe src='<?php echo base_url("assets/pdf/pbk/per_kep_menteri.pdf") ?>' width="650" height="500"></iframe>
									<p>Jika Load PDF Gagal. Klik Link Ini untuk <a href="/pdf/pbk/per_kep_menteri.pdf" target="blank"><strong style="color: black;">Download PDF </strong></a></p>
								<br><hr><br>
								<div class="col-12">
									<div class="row">
										<div class="col-6">
											<strong style="color: black;">AUTHOR :</strong> <span style="color: blue;">Administrator</span>
										</div>
										<div class="col-6">
											<strong style="color: black;">TAGS</strong>
										</div>
									</div>
									<div class="row">
										<div class="col-6">
											<span>Pelayanan Perizinan Berusaha Terintegrasi Secara Elektronik di Bidang Perdagangan</span>
										</div>
										<div class="col-6">
											<button style="color: grey; border: 0px">Aktualita</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- SIDEBAR RIGHT-->
			<div class="sidebar col-lg-4">
				<div class="widget">
					<div class="post-thumbnail-list">
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<span type="button" class="btn btn-primary btn-sm">Aktualita Terpopuler</span>
								<br><br>
								<a href="#">PERATURAN MENTERI PERDAGANGAN RI NOMOR 77 TAHUN 2018</a>
								<span class="post-date">01 Agustus 2018</span>
								<span class="post-category">Views : 1958</span>
							</div>
						</div>
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<a href="#">KEPUTUSAN MENTERI PERINDUSTRIAN DAN PERDAGANGAN NO 86/MPP/KEP/3/2001</a>
								<span class="post-date">04 Maret 2001</span>
								<span class="post-category">Views : 1737</span>
							</div>
						</div>
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<a href="#">PERATURAN MENTERI PERDAGANGAN RI NOMOR 73/M-DAG/PER/9/2017</a>
								<span class="post-date">01 November 2017</span>
								<span class="post-category">Views : 1463</span>
							</div>
						</div>
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<a href="#">PERATURAN MENTERI PERDAGANGAN RI NOMOR 01/M-DAG/PER/3/2005</a>
								<span class="post-date">04 April 2005</span>
								<span class="post-category">Views : 1401</span>
							</div>
						</div>

						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<span type="button" class="btn btn-primary btn-sm">Aktualita Terbaru</span>
								<br><br>
								<a href="#">PERATURAN MENTERI PERDAGANGAN RI NOMOR 77 TAHUN 2018</a>
								<span class="post-date">01 Agustus 2018</span>
								<span class="post-category">Views : 1958</span>
							</div>
						</div>
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<a href="#">KEPUTUSAN MENTERI PERINDUSTRIAN DAN PERDAGANGAN NO 86/MPP/KEP/3/2001</a>
								<span class="post-date">04 Maret 2001</span>
								<span class="post-category">Views : 1737</span>
							</div>
						</div>
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<a href="#">PERATURAN MENTERI PERDAGANGAN RI NOMOR 73/M-DAG/PER/9/2017</a>
								<span class="post-date">01 November 2017</span>
								<span class="post-category">Views : 1463</span>
							</div>
						</div>
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<a href="#">PERATURAN MENTERI PERDAGANGAN RI NOMOR 01/M-DAG/PER/3/2005</a>
								<span class="post-date">04 April 2005</span>
								<span class="post-category">Views : 1401</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- end: SIDEBAR RIGHT-->
		</div>
	</div>
</section>

<!-- FOOTER -->
<?php $this->load->view('include/footer'); ?>
<!-- end: FOOTER -->
