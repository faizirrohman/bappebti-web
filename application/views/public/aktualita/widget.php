<div class="heading-text">
	<h4>Aktualita</h4>
</div>
<div class="post-thumbnail-list" style="overflow: scroll; height: 300px; overflow-x: hidden;">
	<div class="post-thumbnail-entry">
		<div class="post-thumbnail-content">
			<a href="#">Pengumuman Pelaksanaan Ujian Profesi Calon Wakil Pialang Berjangka 8-9 Juli 2021 Di Bandung</a>
			<span class="post-date">28 Mei 2021</span>
		</div>
	</div>
	<div class="post-thumbnail-entry">
		<div class="post-thumbnail-content">
			<a href="#">Pengumuman Nama Peserta Yang Berhak Mengikuti Ujian Profesi Calon Wakil Pialang Perdagangan Berjangka Komoditi Di Jakarta, 29...</a>
			<span class="post-date">28 Mei 2021</span>
		</div>
	</div>
	<div class="post-thumbnail-entry">
		<div class="post-thumbnail-content">
			<a href="#">Pengumuman Pelaksanaan Ujian Profesi Calon Wakil Pialang Berjangka Periode April 2021</a>
			<span class="post-date">28 Mei 2021</span>
		</div>
	</div>
	<div class="post-thumbnail-entry">
		<div class="post-thumbnail-content">
			<a href="#">Pengumuman Pelaksanaan Ujian Profesi Calon Wakil Pialang Berjangka Periode April 2021</a>
			<span class="post-date">28 Mei 2021</span>
		</div>
	</div>
	<div class="post-thumbnail-entry">
		<div class="post-thumbnail-content">
			<a href="#">Pengumuman Pelaksanaan Ujian Profesi Calon Wakil Pialang Berjangka Periode April 2021</a>
			<span class="post-date">28 Mei 2021</span>
		</div>
	</div>
</div>
<br>
<a href="#" class="text-theme"><strong style="color: #589bba !important;">LIHAT SEMUA <i class="fa fa-angle-right "></strong></i></a>
<br><br>
