<!-- HEADER -->
<?php $this->load->view('include/header'); ?>
<!-- end: HEADER -->

<!-- AKTUALITA -->
<section class="sidebar-right background-grey">
	<div class="container">
		<div class="heading-text">
			<!-- BREADCRUMB -->
			<div class="breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="#">Halaman Utama</a></li>
						<li class="breadcrumb-item active" aria-current="page">Aktualita</li>
					</ol>
				</nav>
			</div>
			<!-- end: BREADCRUMB -->
			<hr>
			<h4>Aktualita</h4>
		</div>
		<div class="row">
			<div class="content col-lg-8">
				<form action="" method="post">
					<div class="row">
						<!-- <form action="" method="post"> -->
							<div class="col-lg-5">
								<input type="text" name="" class="form-control background-grey" placeholder="Judul...">
							</div>
							<div class="col-lg-2">
								<button type="submit" id="btn-search" class="form-control" style="background-color: #6cb6d8;"><i style="color: white;" class="icon-search"></i></button>
							</div>
						<!-- </form> -->
					</div>
				</form>
				<br>
				<div id="blog">
					<div class="post-item">
						<div class="post-thumbnail-list">
							<div class="post-thumbnail">
								<div class="post-thumbnail-content">
									<span class="btn btn-primary btn-sm">30 Mei 2000</span>
									<a href="<?php echo base_url('aktualita/detail') ?>">
										<p><strong>PENGUMUMAN PESERTA YANG LULUS UJIAN TERTULIS CALON WAKIL PIALANG BERJANGKA DI BANDUNG</strong></p>
									</a>
								</div>
							</div>
							<hr>
							<div class="post-thumbnail">
								<div class="post-thumbnail-content">
									<span class="btn btn-primary btn-sm">27 Januari 2000</span>
									<a href="#">
										<p><strong>Nama Peserta Yang Berhak Mengikuti Ujian Profesi Calon Wakil Pialang Perdagangan Berjangka Komoditi Di Bandung</strong></p>
									</a>
								</div>
							</div>
							<hr>
							<div class="post-thumbnail">
								<div class="post-thumbnail-content">
									<span class="btn btn-primary btn-sm">20 Mei 2001</span>
									<a href="#">
										<p><strong>Pengumuman Nama Peserta Yang Memenuhi Persyaratan Administrasi Untuk Mengikuti Ujian Profesi Calon Wakil Pialang Perdagangan Berjangka Komoditi di Bandung, 25 November 2021</strong></p>
									</a>
								</div>
							</div>
							<hr>
							<div class="post-thumbnail">
								<div class="post-thumbnail-content">
									<span class="btn btn-primary btn-sm">20 Mei 2001</span>
									<a href="#">
										<p><strong>Pengumuman Pelaksanaan Ujian Profesi Calon Wakil Pialang Berjangka di Jawa Barat</strong></p>
									</a>
								</div>
							</div>
							<hr>
							<div class="post-thumbnail">
								<div class="post-thumbnail-content">
									<span class="btn btn-primary btn-sm">20 Mei 2001</span>
									<a href="#">
										<p><strong>Pengumuman Hasil Ujian Profesi Calon Wakil Pialang Berjangka Angkatan II 2021</strong></p>
									</a>
								</div>
							</div>
							<hr>
							<div class="post-thumbnail">
								<button class="btn btn-outline-primary">LIHAT SEMUA</button>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- SIDEBAR RIGHT-->
			<div class="sidebar col-lg-4">
				<!--Tabs with Posts-->
				<div class="widget ">
					<div class="post-thumbnail-list">
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<span type="button" class="btn btn-primary btn-sm">Aktualita Terpopuler</span>
								<br><br>
								<a href="#">Pengumuman Perusahaan Yang Sudah Memperoleh Tanda Daftar Dari BAPPEBTI Sebagai Calon Pedagang Fisik Aset Kripto</a>
								<span class="post-date">30 Mei 2000</span>
								<span class="post-category">Views : 2895</span>
							</div>
						</div>
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<a href="#">Daftar perusahaan Pedagang Aset Kripto yang terdaftar di Bappebti (Calon Pedagang)</a>
								<span class="post-date">20 Mei 2001</span>
								<span class="post-category">Views : 1915</span>
							</div>
						</div>
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<a href="#">Pemberitahuan Daftar Terduga Teroris dan Organisasi Teroris (DTTOT)</a>
								<span class="post-date">27 Januari 2000</span>
								<span class="post-category">Views : 1563</span>
							</div>
						</div>
						<div class="post-thumbnail-entry">
							<div class="post-thumbnail-content">
								<a href="#">Pengumuman Perusahaan Yang Sudah Memperoleh Tanda Daftar Dari BAPPEBTI Sebagai Calon Pedagang Fisik Aset Kripto</a>
								<span class="post-date">27 Januari 2000</span>
								<span class="post-category">Views : 1563</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- end: SIDEBAR RIGHT-->
		</div>
	</div>
</section>
<!-- end: AKTUALITA -->

<!-- FOOTER -->
<?php $this->load->view('include/footer'); ?>
<!-- end: FOOTER -->
