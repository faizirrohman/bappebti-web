<!-- HEADER -->
<?php $this->load->view('include/header'); ?>
<!-- end: HEADER -->

<div id="slider" class="inspiro-slider slider-halfscreen dots-dark arrows-dark dots-creative" data-fade="true" data-height-xs="360">
	<!-- SLIDE 1 -->
	<div class="slide background-image" data-bg-image="<?php echo base_url('assets/images/bg-home.jpg') ?>">
		<div class="container">
			<div class="slide-captions text-end">
				<h2 class="text-white text-lg">Bappebti, Aspebtindo, dan ICDX Selenggarakan P4WPB untuk Meningkatkan Kualitas Wakil Pialang Berjangka</h2>
			</div>
		</div>
	</div>
	<!-- end: SLIDE 1 -->

	<!-- SLIDE 2 -->
	<div class="slide background-image" data-bg-image="<?php echo base_url('assets/images/bg-home.jpg') ?>">
		<div class="container">
			<div class="slide-captions text-end">
				<h2 class="text-white text-lg">Bappebti, Aspebtindo, dan ICDX Selenggarakan P4WPB untuk Meningkatkan Kualitas Wakil Pialang Berjangka</h2>
			</div>
		</div>
	</div>
	<!-- end: SLIDE 2 -->
</div>

<section id="page-content" style="background-color: #103A56;">
	<div class="container">
		<div class="grid-system-demo">
			<div class="row">
				<div class="col-lg-3"><span class="grid-col-demo">Harga Bursa 01 September 2021 ></span> </div>
				<div class="col-lg-9"><span class="grid-col-demo" style="color:white; background-color:#103A56; text-align: left;"> Komoditas TINPB100 -
						Jenis SPOT Jun21 - Lokasi Jakarta ICDX - Harga 0 US$/ton </span> </div>
			</div>
		</div>
		<!-- LAYANAN DAN PRODUK -->
		<div class="col-lg-12">
			<?php $this->load->view('public/produk_layanan/widget'); ?>
		</div>
		<!-- end: LAYANAN DAN PRODUK -->

		<!-- <ul class="list-unstyled mb-0 row gutters-5">
			<li class="minw-0 col-4 col-md mt-3">
				<a href="" class="d-block rounded bg-white p-2 text-reset shadow-sm">
					<img src="img/layanan/1.png" class="img-fluid" alt="">
				</a>
			</li>
			<li class="minw-0 col-4 col-md mt-3">
				<a href="" class="d-block rounded bg-white p-2 text-reset shadow-sm">
					<img src="img/layanan/2.png" class="img-fluid" alt="">
				</a>
			</li>
			<li class="minw-0 col-4 col-md mt-3">
				<a href="" class="d-block rounded bg-white p-2 text-reset shadow-sm">
					<img src="img/layanan/3.png" class="img-fluid" alt="">
				</a>
			</li>
			<li class="minw-0 col-4 col-md mt-3">
				<a href="" class="d-block rounded bg-white p-2 text-reset shadow-sm">
					<img src="img/layanan/4.png" class="img-fluid" alt="">
				</a>
			</li>
			<li class="minw-0 col-4 col-md mt-3">
				<a href="" class="d-block rounded bg-white p-2 text-reset shadow-sm">
					<img src="img/layanan/5.png" class="img-fluid" alt="">
				</a>
			</li>
			<li class="minw-0 col-4 col-md mt-3">
				<a href="" class="d-block rounded bg-white p-2 text-reset shadow-sm">
					<img src="img/layanan/6.png" class="img-fluid" alt="">
				</a>
			</li>
			<li class="minw-0 col-4 col-md mt-3">
				<a href="" class="d-block rounded bg-white p-2 text-reset shadow-sm">
					<img src="img/layanan/7.png" class="img-fluid" alt="">
				</a>
			</li>
			<li class="minw-0 col-4 col-md mt-3">
				<a href="" class="d-block rounded bg-white p-2 text-reset shadow-sm">
					<img src="img/layanan/8.png" class="img-fluid" alt="">
				</a>
			</li>
			<li class="minw-0 col-4 col-md mt-3">
				<a href="" class="d-block rounded bg-white p-2 text-reset shadow-sm">
					<img src="img/layanan/9.png" class="img-fluid" alt="">
				</a>
			</li>
		</ul> -->
	</div>
</section>

<section class="background-grey">
	<div class="container">
		<div id="blog" class="grid-layout post-3-columns m-b-30" data-item="post-item">
			<div class="post-item border">
				<div class="post-item-wrap">
					<div class="post-image">
						<a href="#">
							<img alt="" style="height: 200px;" src="<?php echo base_url('assets/images/12.jpg') ?>" class="img-fluid rounded">
						</a>
						<span class="post-meta-category" style="top: 160px; right: 238px;"><a href="">Learn
								more</a></span>
					</div>
				</div>
			</div>
			<div class="post-item border">
				<div class="post-item-wrap">
					<div class="post-image">
						<a href="#">
							<img alt="" style="height: 200px;" src="<?php echo base_url('assets/images/12.jpg') ?>" class="img-fluid rounded">
						</a>
						<span class="post-meta-category" style="top: 160px; right: 238px;"><a href="">Learn
								more</a></span>
					</div>
				</div>
			</div>
			<div class="post-item border">
				<div class="post-item-wrap">
					<div class="post-image">
						<a href="#">
							<img alt="" style="height: 200px;" src="<?php echo base_url('assets/images/12.jpg') ?>" class="img-fluid rounded">
						</a>
						<span class="post-meta-category" style="top: 160px; right: 238px;"><a href="">Learn
								more</a></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="page-content">
	<div id="blog">
		<div class="container">
			<!-- SIARAN PERS -->
			<div id="blog">
				<?php $this->load->view('public/siaran_pers/widget'); ?>
			</div>
			<!-- end: SIARAN PERS -->
		</div>
	</div>
</section>

<section class="background-grey">
	<div class="container">
		<div class="row">
			<!-- AKTUALITA -->
			<div class="col-lg-4">
				<?php $this->load->view('public/aktualita/widget'); ?>
			</div>
			<!-- end: AKTUALITA -->

			<!-- PENGATURAN BARU -->
			<div class="col-lg-4">
				<?php $this->load->view('public/peraturan_pemerintah/widget'); ?>
			</div>
			<!-- end: PENGATURAN BARU -->

			<!-- KEGIATAN -->
			<div class="col-lg-4">
				<?php $this->load->view('public/kegiatan/widget'); ?>
			</div>
			<!-- end: KEGIATAN -->
		</div>
		<br>
		<br>
		<div class="row">
			<div class="col-lg-12">
				<div class="post-thumbnail-list">
					<div class="post-image">
						<img alt="" class="img-fluid" src="<?php echo base_url('assets/images/1200x90.png') ?>">
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="page-content">
	<div id="blog">
		<div class="container">
			<!-- POJOK MEDIA -->
			<div id="blog">
				<?php $this->load->view('public/pojok_media/widget'); ?>
			</div>
			<!-- end: POJOK MEDIA -->
		</div>
	</div>
</section>

<section class="background-grey">
	<div class="container">
		<div class="heading-text">
			<h4>Publikasi</h4>
		</div>
		<div class="col-12">
			<div id="blog" class="grid-layout post-3-columns m-b-30" data-item="post-item">
				<!-- ANNUAL REPORT BAPPEBTI -->
				<div class="post-item">
					<?php $this->load->view('public/annual_report_bappebti/widget') ?>
				</div>
				<!-- end: ANNUAL REPORT BAPPEBTI -->

				<!-- BULLETIN BERJANGKA -->
				<div class="post-item">
					<?php $this->load->view('public/bulletin_perdagangan_berjangka/widget') ?>
				</div>
				<!-- end: BULLETIN BERJANGKA -->
				
				<!-- STATIK PERDAGANGAN BERJANGKA -->
				<div class="post-item">
					<?php $this->load->view('public/bulletin_statistik_perdagangan_berjangka/widget') ?>
				</div>
				<!-- end: STATIK PERDAGANGAN BERJANGKA -->
			</div>
		</div>
		
		<div class="col-12">
			<div class="row">
				<div id="blog" class="grid-layout post-2-columns m-b-20" data-item="post-item">
					<!-- SOCIAL MEDIA FACEBOOK -->
					<div class="col-5">
						<?php $this->load->view('public/social/facebook/widget') ?>
					</div>
					<!-- enf: SOCIAL MEDIA FACEBOOK -->

					<!-- DATA KURS TENGAH -->
					<div class="col-7">
						<?php $this->load->view('public/chart/widget') ?>
					</div>
					<!-- end: DATA KURS TENGAH -->
				</div>
			</div>
		</div>
	</div>
</section>

<section id="page-content">
	<div class="container">
		<!-- KERJA SAMA BAPPEBTI -->
		<div class="row">
			<?php $this->load->view('public/bappebti_client/widget') ?>
		</div>
		<!-- end: KERJA SAMA BAPPEBTI -->
	</div>
</section>

<!-- VIDEO -->
<section class="sidebar-right" style="background-color: rgb(2 39 78);">
	<div class="container">
		<?php $this->load->view('public/video/widget') ?>
	</div>
</section>
<!-- end: VIDEO -->

<section id="page-content">
	<!-- INFORMASI BAPPEBTI -->
	<div class="container">
		<?php $this->load->view('public/berita_sidebar/widget') ?>
	</div>
	<!-- end: INFORMASI BAPPEBTI -->
</section>

<!-- FOOTER -->
<?php $this->load->view('include/footer'); ?>
<!-- end: FOOTER -->
