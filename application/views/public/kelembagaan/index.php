<!-- HEADER -->
<?php $this->load->view('include/header'); ?>
<!-- end: HEADER -->

<!-- BACKGROUND KELEMBAGAAN -->
<section class="parallax text-light halfscreen" data-bg-parallax="<?php echo base_url('assets/images/bg-all.png') ?>">
	<div class="container">
		<div class="container-fullscreen">
			<div class="text-middle text-center text-end">
				<h1 class="text-large" data-animate="animate__fadeInDown">Kelembagaan</h1>
			</div>
		</div>
	</div>
</section>
<!-- end: BACKGROUND KELEMBAGAAN -->

<!-- KELEMBAGAAN -->
<section class="background-grey">
	<div class="container">
		<div class="col-12">
			<div class="row">
				<div id="blog" class="grid-layout" data-item="post-item">
					<div class="col-5">
						<div class="post-item">
							<div class="post-item-wrap" style="border-radius: 10px;">
								<div class="post-item-description" style="padding: 40px;">
									<h2>Struktur Industri Perdagangan Berjangka Komoditi</h2>
									<hr>
									<img alt="" src="<?php echo base_url('assets/images/kelembagaan/sipbk.png') ?>" class="img-responsive">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12">
			<div class="row">
				<div id="blog" class="grid-layout" data-item="post-item">
					<div class="col-5">
						<div class="post-item">
							<div class="post-item-wrap" style="border-radius: 10px;">
								<div class="post-item-description" style="padding: 40px;">
									<h2>Struktur Industri Sistem Resi Gudang</h2>
									<hr>
									<img alt="" src="<?php echo base_url('assets/images/kelembagaan/sisrg.png') ?>">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- end: KELEMBAGAAN -->

<!-- FOOTER -->
<?php $this->load->view('include/footer'); ?>
<!-- end: FOOTER -->
