<!-- HEADER -->
<?php $this->load->view('include/header'); ?>
<!-- end: HEADER -->

<!-- BACKGROUND KEWENANGAN -->
<section class="parallax text-light halfscreen" data-bg-parallax="<?php echo base_url('assets/images/bg-all.png') ?>">
	<div class="container">
		<div class="container-fullscreen">
			<div class="text-middle text-center text-end">
				<h1 class="text-large" data-animate="animate__fadeInDown">Kewenangan</h1>
			</div>
		</div>
	</div>
</section>
<!-- end: BACKGROUND KEWENANGAN -->

<!-- KEWENANGAN -->
<section class="background-grey">
	<div class="container">
		<div id="blog" class="grid-layout post-1-columns m-b-30" data-item="post-item">
			<div class="post-item">
				<div class="post-item-wrap" style="border-radius: 8px;">
					<div class="post-item-description" style="padding: 35px;">
						<h2 style="margin: 15px;">Tugas dan Kewenangan Bappebti</h2>
						<ol>
							<li>
								<p style="margin: 15px;">Menerbitkan izin usaha bagi Bursa Berjangka, Lembaga Kliring Berjangka, Pialang Berjangka, Penasihat Berjangka dan Pengelola Sentra Dana Berjangka; izin bagi perorangan untuk menjadi Wakil Pialang Berjangka,
									Wakil Penasihat Berjangka, dan Wakil Pengelola Sentra Dana Berjangka; Sertifikat pendaftaran bagi Pedagang Berjangka; serta persetujuan bagi Pialang Berjangka untuk menyalurkan amanat Nasabah Berjangka
									keluar Negeri dan bagi bank penitipan dana yang terkait dengan perdagangan berjangka.
								</p>
							</li>
							<li>
								<p style="margin: 15px;">Mengesahkan Peraturan dan Tata Tertib (Rules dan Regulations) Bursa Berjangka dan Lembaga Kliring Berjangka serta Kontrak berjangka yang akan diperdagangkan di Bursa Berjangka,
									termasuk perubahannya.
								</p>
							</li>
							<li>
								<p style="margin: 15px;">Memastikan agar Bursa Berjangka dan Lembaga Kliring Berjangka melaksanakan semua ketentuan dan peraturan yang telah ditetapkan serta melakukan pengawasan yang intensif dan pengenaan sanksi tegas
									terhadap pelanggarannya.
								</p>
							</li>
							<li>
								<p style="margin: 15px;">Menetapkan jumlah maksimum posisi terbuka yang dapat dimiliki atau dikuasai oleh setiap pihak dan batas jumlah posisi terbuka yang wajib dilaporkan.
								</p>
							</li>
							<li>
								<p style="margin: 15px;">Menetapkan Daftar Bursa Berjangka Kontrak Berjangka luar Negeri yang dapat menjadi tujuan penyaluran amanat Nasabah dalam Negeri.</p>
							</li>
							<li>
								<p style="margin: 15px;">Melakukan pemeriksaan terhadap setiap pihak yang memiliki izin dan memerintahkan pemeriksaan serta penyidikan terhadap pihak yang diduga melakukan pelanggaran terhadap ketentuan perundang-undangan dibidang
									perdagangan berjangka.</p>
							</li>
							<li>
								<p style="margin: 15px;">Mewajibkan kepada setiap pihak untuk menghentikan atau memperbaiki iklan atau promosi tentang perdagangan berjangka yang dapat menyesatkan.</p>
							</li>
							<li>
								<p style="margin: 15px;">Membentuk sarana penyelesaian masalah yang berkaitan dengan kegiatan perdagangan berjangka.</p>
							</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- end: KEWENANGAN -->

<!-- FOOTER -->
<?php $this->load->view('include/footer'); ?>
<!-- end: FOOTER -->
