<div class="row ">
	<div class="col-lg-6">
		<div class="heading-text">
			<h4>Informasi Bappebti</h4>
		</div>
	</div>
</div>  
<div id="portfolio" class="grid-layout portfolio-3-columns" data-margin="20">
	<div class="portfolio-item no-overlay img-zoom">
		<div class="portfolio-item-wrap">
			<div class="portfolio-image">
				<a href="#"><img src="<?php echo base_url('assets/images/infobappebti/1.png') ?>" alt=""></a>
			</div>
		</div>
	</div>
	<div class="portfolio-item no-overlay img-zoom">
		<div class="portfolio-item-wrap">
			<div class="portfolio-image">
				<a href="#"><img src="<?php echo base_url('assets/images/infobappebti/2.png') ?>" alt=""></a>
			</div>
		</div>
	</div>
	<div class="portfolio-item no-overlay img-zoom">
		<div class="portfolio-item-wrap">
			<div class="portfolio-image">
				<a href="#"><img src="<?php echo base_url('assets/images/infobappebti/3.png') ?>" alt=""></a>
			</div>
		</div>
	</div>

	<div class="portfolio-item no-overlay img-zoom">
		<div class="portfolio-item-wrap">
			<div class="portfolio-image">
				<a href="#"><img src="<?php echo base_url('assets/images/infobappebti/4.png') ?>" alt=""></a>
			</div>
		</div>
	</div>
	<div class="portfolio-item no-overlay img-zoom">
		<div class="portfolio-item-wrap">
			<div class="portfolio-image">
				<a href="#"><img src="<?php echo base_url('assets/images/infobappebti/5.png') ?>" alt=""></a>
			</div>
		</div>
	</div>
	<div class="portfolio-item no-overlay img-zoom">
		<div class="portfolio-item-wrap">
			<div class="portfolio-image">
				<a href="#"><img src="<?php echo base_url('assets/images/infobappebti/6.png') ?>" alt=""></a>
			</div>
		</div>
	</div>
</div>
