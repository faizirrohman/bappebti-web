<!-- HEADER -->
<?php $this->load->view('include/header'); ?>
<!-- end: HEADER -->

<!-- BACKGROUND ORGANISASI -->
<section class="parallax text-light halfscreen" data-bg-parallax="<?php echo base_url('assets/images/bg-all.png') ?>">
	<div class="container">
		<div class="container-fullscreen">
			<div class="text-middle text-center text-end">
				<h1 class="text-large" data-animate="animate__fadeInDown">Organisasi</h1>
			</div>
		</div>
	</div>
</section>
<!-- end: BACKGROUND ORGANISASI -->

<!-- FOOTER -->
<?php $this->load->view('include/footer'); ?>
<!-- end: FOOTER -->
