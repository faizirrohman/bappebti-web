<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title><?php echo $title ?></title>
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets/images/logo_bappebti.png') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/backend/vendor/owl-carousel/css/owl.carousel.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/backend/vendor/owl-carousel/css/owl.theme.default.min.css') ?>">
    <link href="<?php echo base_url('assets/backend/vendor/jqvmap/css/jqvmap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/backend/css/style.css') ?>" rel="stylesheet">
</head>

<body>
    <div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>

    <!-- MAIN WRAPPER -->
    <div id="main-wrapper">
		<!-- NAV HEADER -->
        <div class="nav-header">
            <a href="index.html" class="brand-logo">
                <img class="logo-abbr" src="<?php echo base_url('assets/backend/images/logo.png') ?>" alt="">
                <img class="logo-compact" src="<?php echo base_url('assets/backend/images/logo-text.png') ?>" alt="">
                <img class="brand-title" src="<?php echo base_url('assets/backend/images/logo-text.png') ?>" alt="">
            </a>
            <div class="nav-control">
                <div class="hamburger">
                    <span class="line"></span><span class="line"></span><span class="line"></span>
                </div>
            </div>
        </div>
		<!-- end: NAV HEADER -->

        <!-- HEADER -->
        <div class="header">
            <div class="header-content">
                <nav class="navbar navbar-expand">
                    <div class="collapse navbar-collapse justify-content-between">
                        <div class="header-left">
                            <div class="search_bar dropdown">
                                <span class="search_icon p-3 c-pointer" data-toggle="dropdown">
                                    <i class="mdi mdi-magnify"></i>
                                </span>
                                <div class="dropdown-menu p-0 m-0">
                                    <form>
                                        <input class="form-control" type="search" placeholder="Search" aria-label="Search">
                                    </form>
                                </div>
                            </div>
                        </div>

                        <ul class="navbar-nav header-right">
                            <li class="nav-item dropdown header-profile">
                                <a class="nav-link" href="#" role="button" data-toggle="dropdown">
                                    <i class="mdi mdi-account"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="./app-profile.html" class="dropdown-item">
                                        <i class="icon-user"></i>
                                        <span class="ml-2">Profile </span>
                                    </a>
                                    <a href="./email-inbox.html" class="dropdown-item">
                                        <i class="icon-envelope-open"></i>
                                        <span class="ml-2">Inbox </span>
                                    </a>
                                    <a href="./page-login.html" class="dropdown-item">
                                        <i class="icon-key"></i>
                                        <span class="ml-2">Logout </span>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!-- end: HEADER -->

        <!-- SIDEBAR -->
        <div class="quixnav">
            <div class="quixnav-scroll">
                <ul class="metismenu" id="menu">
					<li>
						<a href="" aria-expanded="false">
							<i class="icon icon-app-store"></i><span class="nav-text">Dashboard</span>
						</a>
                    </li>
					<li>
						<a href="javascript:void()" aria-expanded="false">
							<i class="icon icon-single-04"></i><span class="nav-text">User</span>
						</a>
                    </li>
					<li>
						<a href="javascript:void()" aria-expanded="false">
							<i class="icon icon-plug"></i><span class="nav-text">Setting</span>
						</a>
                    </li>
                    <li><a class="has-arrow" href="javascript:void()" aria-expanded="false">
						<i class="icon icon-form"></i><span class="nav-text">Post</span></a>
                        <ul aria-expanded="false">
                            <li><a href="#">All Post</a></li>
                            <li><a href="#">Add New</a></li>
                            <li><a href="#">Categories</a></li>
                            <li><a href="#">Tags</a></li>
                        </ul>
                    </li>
                    <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i
                                class="icon icon-layout-25"></i><span class="nav-text">Media</span></a>
                        <ul aria-expanded="false">
                            <li><a href="#">Library</a></li>
                            <li><a href="#">Add New</a></li>
                        </ul>
                    </li>

                    <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i
                                class="icon icon-single-copy-06"></i><span class="nav-text">Pages</span></a>
                        <ul aria-expanded="false">
                            <li><a href="#">All Pages</a></li>
                            <li><a href="#">Add New</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- end: SIDEBAR -->

        <!-- CONTENT -->
        <!-- <div class="content-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="stat-widget-two card-body">
                                <div class="stat-content">
                                    <div class="stat-text">Today Expenses </div>
                                    <div class="stat-digit"> <i class="fa fa-usd"></i>8500</div>
                                </div>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success w-85" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="stat-widget-two card-body">
                                <div class="stat-content">
                                    <div class="stat-text">Income Detail</div>
                                    <div class="stat-digit"> <i class="fa fa-usd"></i>7800</div>
                                </div>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary w-75" role="progressbar" aria-valuenow="78" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="stat-widget-two card-body">
                                <div class="stat-content">
                                    <div class="stat-text">Task Completed</div>
                                    <div class="stat-digit"> <i class="fa fa-usd"></i> 500</div>
                                </div>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-warning w-50" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="stat-widget-two card-body">
                                <div class="stat-content">
                                    <div class="stat-text">Task Completed</div>
                                    <div class="stat-digit"> <i class="fa fa-usd"></i>650</div>
                                </div>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-danger w-65" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->

		<div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles mx-0">
                    <div class="col-sm-6 p-md-0">
                        <div class="welcome-text">
                            <h4>Hi, welcome back!</h4>
                            <p class="mb-1">Validation</p>
                        </div>
                    </div>
                    <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Form</a></li>
                            <li class="breadcrumb-item active"><a href="javascript:void(0)">Validation</a></li>
                        </ol>
                    </div>
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Form Validation</h4>
                            </div>
                            <div class="card-body">
                                <div class="form-validation">
                                    <form class="form-valide" action="#" method="post">
                                        <div class="row">
                                            <div class="col-xl-6">
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label" for="val-username">Name
                                                        <span class="text-danger">*</span>
                                                    </label>
                                                    <div class="col-lg-6">
                                                        <input type="text" class="form-control form-control-lg" id="val-username" name="val-username" placeholder="Enter a username..">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label" for="val-email">Title <span
                                                            class="text-danger">*</span>
                                                    </label>
                                                    <div class="col-lg-6">
                                                        <input type="text" class="form-control form-control-lg" id="val-email" name="val-email" placeholder="Your valid email..">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label" for="val-password">Description
                                                        <span class="text-danger">*</span>
                                                    </label>
                                                    <div class="col-lg-6">
                                                        <input type="password" class="form-control form-control-lg" id="val-password" name="val-password" placeholder="Choose a safe one..">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label" for="val-confirm-password">Confirm Password <span
                                                            class="text-danger">*</span>
                                                    </label>
                                                    <div class="col-lg-6">
                                                        <input type="password" class="form-control form-control-lg" id="val-confirm-password" name="val-confirm-password" placeholder="..and confirm it!">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label" for="val-suggestions">Suggestions <span
                                                            class="text-danger">*</span>
                                                    </label>
                                                    <div class="col-lg-6">
                                                        <textarea class="form-control" id="val-suggestions" name="val-suggestions" rows="5" placeholder="What would you like to see?"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end: CONTENT -->

        <!-- FOOTER -->
        <div class="footer">
            <div class="copyright">
                <p>Copyright © Designed &amp; Developed by <a href="#" target="_blank">Quixkit</a> 2019</p>
                <p>Distributed by <a href="https://themewagon.com/" target="_blank">Themewagon</a></p> 
            </div>
        </div>
		<!-- end: FOOTER -->
    </div>
	<!-- end: MAIN WRAPPER -->

    <!-- Required vendors -->
    <script src="<?php echo base_url('assets/backend/vendor/global/global.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/backend/js/quixnav-init.js') ?>"></script>
    <script src="<?php echo base_url('assets/backend/js/custom.min.js') ?>"></script>

    <!-- Vectormap -->
    <script src="<?php echo base_url('assets/backend/vendor/raphael/raphael.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/backend/vendor/morris/morris.min.js') ?>"></script>

    <script src="<?php echo base_url('assets/backend/vendor/circle-progress/circle-progress.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/backend/vendor/chart.js/Chart.bundle.min.js') ?>"></script>

    <script src="<?php echo base_url('assets/backend/vendor/gaugeJS/dist/gauge.min.js') ?>"></script>

    <!--  flot-chart js -->
    <script src="<?php echo base_url('assets/backend/vendor/flot/jquery.flot.js') ?>"></script>
    <script src="<?php echo base_url('assets/backend/vendor/flot/jquery.flot.resize.js') ?>"></script>

    <!-- Owl Carousel -->
    <script src="<?php echo base_url('assets/backend/vendor/owl-carousel/js/owl.carousel.min.js') ?>"></script>

    <!-- Counter Up -->
    <script src="<?php echo base_url('assets/backend/vendor/jqvmap/js/jquery.vmap.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/backend/vendor/jqvmap/js/jquery.vmap.usa.js') ?>"></script>
    <script src="<?php echo base_url('assets/backend/vendor/jquery.counterup/jquery.counterup.min.js') ?>"></script>

    <script src="<?php echo base_url('assets/backend/js/dashboard/dashboard-1.js') ?>"></script>
</body>

</html>
