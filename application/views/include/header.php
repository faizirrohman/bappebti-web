<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="INSPIRO" />
    <meta name="description" content="Themeforest Template Polo, html template">
    <link rel="icon" type="image/png" href="<?php echo base_url('assets/images/logo_bappebti.png') ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title ?></title>
    <link href="<?php echo base_url('assets/css/plugins.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/style.css') ?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/event-style.css') ?>" media="screen" />
</head>

<body>
    <!-- BODY INNER -->
    <div class="body-inner">
        <!-- HEADER -->
        <header id="header" data-fullwidth="true">
            <div class="header-inner">
                <div class="container">
                    <!-- LOGO -->
                    <div id="logo">
                        <a href="<?php echo base_url('home') ?>">
                            <img class="logo-default" src="<?php echo base_url('assets/images/logo_bappebti.png') ?>">
                            <img class="logo-dark" src="<?php echo base_url('assets/images/logo_bappebti.png') ?>">
                        </a>
                    </div>
                    <!--End: LOGO -->
					
                    <!-- SEARCH -->
                    <div id="search"><a id="btn-search-close" class="btn-search-close" aria-label="Close search form"><i class="icon-x"></i></a>
                        <form class="search-form" action="search-results-page.html" method="get">
                            <input class="form-control" name="q" type="text" placeholder="Type & Search..." />
                            <span class="text-muted">Start typing & press "Enter" or "ESC" to close</span>
                        </form>
                    </div>
                    <!-- end: SEARCH -->

                    <!-- HEADER EXTRAS -->
                    <div class="header-extras">
                        <ul>
                            <li class="social-facebook">
                                <a href="http://www.facebook.com/bappebti.kementerianperdagangan" target="blank"><i class="fab fa-facebook-f"></i></a>
                            </li>
                            <li class="social-twitter">
                                <a href="http://twitter.com/InfoBappebti" target="blank"><i class="fab fa-twitter"></i></a>
                            </li>
                            <li>
                                <div class="p-dropdown"><strong><a href="#">EN</a></strong>
                                    <ul class="p-dropdown-content">
                                        <li><a href="#">Indonesia</a></li>
                                        <li><a href="#">English</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <!--end: HEADER EXTRAS -->

                    <!-- NAVIGATION RESPONSIVE TRIGGER -->
                    <div id="mainMenu-trigger"> <a class="lines-button x"><span class="lines"></span></a> </div>
                    <!--end: NAVIGATION RESPONSIVE TRIGGER -->

                    <!-- NAVIGATION -->
                    <div id="mainMenu" class="menu-center menu-lowercase">
                        <div class="container">
                            <nav>
                                <ul>
                                    <li><a href="<?php echo base_url('home') ?>">Home</a></li>
                                    <li class="dropdown"><a href="#">Profil <i class="fa fa-angle-down"></i></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="<?php echo base_url('organisasi') ?>"><i class="fa fa-angle-right"></i>Organisasi</a></li>
                                            <li><a href="<?php echo base_url('kewenangan') ?>"><i class="fa fa-angle-right"></i>Kewenangan</a></li>
                                            <li><a href="<?php echo base_url('kelembagaan') ?>"><i class="fa fa-angle-right"></i>Kelembagaan</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown mega-menu-item"><a href="#">Peraturan <i class="fa fa-angle-down"></i></a>
                                        <ul class="dropdown-menu">
                                            <li class="mega-menu-content">
                                                <div class="row">
                                                    <div class="col-lg-2-5">
                                                        <ul>
                                                            <li class="mega-menu-title">Perdagangan Berjangka Komoditi</li>
                                                            <li><a href="<?php echo base_url('pbk/undang_undang') ?>"><i class="fa fa-angle-right"></i>Undang - Undang</a></li>
                                                            <li><a href="<?php echo base_url('pbk/peraturan_pemerintah') ?>"><i class="fa fa-angle-right"></i>Peraturan Pemerintah</a></li>
                                                            <li><a href=""><i class="fa fa-angle-right"></i>Kepres</a></li>
                                                            <li><a href="<?php echo base_url('pbk/per_kep_menteri') ?>"><i class="fa fa-angle-right"></i>Kep./ Per. Menteri</a></li>
                                                            <li><a href="<?php echo base_url('pbk/sk_kep_kepala_bappebti') ?>"><i class="fa fa-angle-right"></i>SK./ Kep. Kepala Bappebti</a></li>
                                                            <li><a href="<?php echo base_url('pbk/edaran_kepala_bappebti') ?>"><i class="fa fa-angle-right"></i>Edaran Kepala Bappebti</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-2-5">
                                                        <ul>
                                                            <li class="mega-menu-title">Sistem Resi Gudang</li>
                                                            <li><a href="#"><i class="fa fa-angle-right"></i>Undang - Undang</a></li>
                                                            <li><a href="#"><i class="fa fa-angle-right"></i>Peraturan Pemerintah</a></li>
                                                            <li><a href="#"><i class="fa fa-angle-right"></i>Kepres</a></li>
                                                            <li><a href="#"><i class="fa fa-angle-right"></i>Kep./ Per. Menteri</a></li>
                                                            <li><a href="#"><i class="fa fa-angle-right"></i>SK./ Kep. Kepala Bappebti</a></li>
                                                            <li><a href="#"><i class="fa fa-angle-right"></i>Edaran Kepala Bappebti</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-2-5">
                                                        <ul>
                                                            <li class="mega-menu-title">Pasar Lelang</li>
                                                            <li><a href="#"><i class="fa fa-angle-right"></i>Undang - Undang</a></li>
                                                            <li><a href="#"><i class="fa fa-angle-right"></i>Peraturan Pemerintah</a></li>
                                                            <li><a href="#"><i class="fa fa-angle-right"></i>Kepres</a></li>
                                                            <li><a href="#"><i class="fa fa-angle-right"></i>Kep./ Per. Menteri</a></li>
                                                            <li><a href="#"><i class="fa fa-angle-right"></i>SK./ Kep. Kepala Bappebti</a></li>
                                                            <li><a href="#"><i class="fa fa-angle-right"></i>Edaran Kepala Bappebti</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="dropdown mega-menu-item"><a href="#">Pelaku Pasar <i class="fa fa-angle-down"></i></a>
                                        <ul class="dropdown-menu">
                                            <li class="mega-menu-content">
                                                <div class="row">
                                                    <div class="col-lg-2-5">
                                                        <ul>
                                                            <li class="mega-menu-title">PBK</li>
                                                            <li><a href="pialang-berjangka.html"><i class="fa fa-angle-right"></i>Pialang Berjangka</a></li>
                                                            <li><a href="#"><i class="fa fa-angle-right"></i>Pialang Berjangka yang Dicabut Izinnya</a></li>
                                                            <li><a href="#"><i class="fa fa-angle-right"></i>Wakil Pialang Berjangka</a></li>
                                                            <li><a href="#"><i class="fa fa-angle-right"></i>Pedagang Berjangka</a></li>
                                                            <li><a href="#"><i class="fa fa-angle-right"></i>Kantor Cabang Pialang Berjangka</a></li>
                                                            <li><a href="#"><i class="fa fa-angle-right"></i>Edaran Kepala Bappebti</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-2-5">
                                                        <ul>
                                                            <li class="mega-menu-title">SRG</li>
                                                            <li><a href="#"><i class="fa fa-angle-right"></i>Kelembagaan</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-2-5">
                                                        <ul>
                                                            <li class="mega-menu-title">Pasar Lelang</li>
                                                            <li><a href="#"><i class="fa fa-angle-right"></i>Daftar Penyelenggara Pasar Lelang</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-2-5">
                                                        <ul>
                                                            <li class="mega-menu-title">Pedagang Fisik</li>
                                                            <li><a href="#"><i class="fa fa-angle-right"></i>Calon Pedagang Fisik Aset Kripto</a></li>
                                                            <li><a href="#"><i class="fa fa-angle-right"></i>Pedagang TImah</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>

                                    <li class="dropdown mega-menu-item"><a href="#">Info Bappebti <i class="fa fa-angle-down"></i></a>
                                        <ul class="dropdown-menu">
                                            <li class="mega-menu-content">
                                                <div class="row">
                                                    <div class="col-lg-2-5">
                                                        <ul>
                                                            <li class="mega-menu-title">Ujian Profesi</li>
                                                            <li><a href="#"><i class="fa fa-angle-right"></i>Ujian Profesi</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-2-5">
                                                        <ul>
                                                            <li class="mega-menu-title">Publikasi</li>
                                                            <li><a href="<?php echo base_url('bulletin_perdagangan_berjangka') ?>"><i class="fa fa-angle-right"></i>Bulletin Perdagangan Berjangka</a></li>
                                                            <li><a href="<?php echo base_url('bulletin_statistik_perdagangan_berjangka') ?>"><i class="fa fa-angle-right"></i>Bulletin Statistik Perdagangan</a></li>
                                                            <li><a href="<?php echo base_url('annual_report_bappebti') ?>"><i class="fa fa-angle-right"></i>Annual Report Bappebti</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-2-5">
                                                        <ul>
                                                            <li class="mega-menu-title">Edukasi</li>
                                                            <li><a href="#"><i class="fa fa-angle-right"></i>Glosasary</a></li>
                                                            <li><a href="#"><i class="fa fa-angle-right"></i>Brosur / Leaflet</a></li>
                                                            <li><a href="#"><i class="fa fa-angle-right"></i>Artikel</a></li>
                                                            <li><a href="#"><i class="fa fa-angle-right"></i>Bahan Literasi</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="dropdown mega-menu-item"><a href="#">Berita <i class="fa fa-angle-down"></i></a>
                                        <ul class="dropdown-menu">
                                            <li class="mega-menu-content">
                                                <div class="row">
                                                    <div class="col-lg-2-5">
                                                        <ul>
                                                            <li class="mega-menu-title">Berita</li>
                                                            <li><a href="<?php echo base_url('siaran_pers') ?>"><i class="fa fa-angle-right"></i>Siaran Pers</a></li>
                                                            <li><a href="portfolio-3.html"><i class="fa fa-angle-right"></i>Berita Photo</a></li>
                                                            <li><a href="<?php echo base_url('aktualita') ?>"><i class="fa fa-angle-right"></i>Aktualita</a></li>
                                                            <li><a href="portfolio-5.html"><i class="fa fa-angle-right"></i>Pojok Media</a></li>
                                                            <li><a href="portfolio-wide-3.html"><i class="fa fa-angle-right"></i>Wide version</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-2-5">
                                                        <ul>
                                                            <li class="mega-menu-title">Harga Komoditi</li>
                                                            <li><a href="portfolio-masonry-2.html"><i class="fa fa-angle-right"></i>Forward - Futures - Spot</a></li>
                                                            <li><a href="http://infoharga.bappebti.go.id/" target="blank"><i class="fa fa-angle-right"></i>Kontributor Sentra Produksi</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-2-5">
                                                        <ul>
                                                            <li class="mega-menu-title">Info Komoditi</li>
                                                            <li><a href="portfolio-filter-styles.html"><i class="fa fa-angle-right"></i>Info Komoditi</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-2-5">
                                                        <ul>
                                                            <li class="mega-menu-title">Kerja Sama</li>
                                                            <li><a href="portfolio-sidebar-left.html"><i class="fa fa-angle-right"></i>Nota Kesepahaman</a></li>
                                                            <li><a href="portfolio-sidebar-right.html"><i class="fa fa-angle-right"></i>Satuan Tugas</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('hubungi_kami') ?>">Hubungi Kami</a>
                                    </li>
                                    <li>
                                        <a id="btn-search" href="#"> <i class="icon-search"></i></a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <!--end: NAVIGATION -->
                </div>
            </div>
        </header>
        <!-- end: HEADER -->
