		<!-- FOOTER -->
		<footer id="footer" class="inverted" style="background-color:#103A56;">
            <div class="footer-content">
                <div class="container">
                    <div class="row">
                        <div></div>
                        <div class="col-xl-4 col-lg-4 col-md-12">
                            <a href="index.html">
                                <img class="logo-default" style="width: 230px; height: 60px;" src="<?php echo base_url('assets/images/logo-footer.png') ?>">
                            </a>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3">
                            <div class="widget">
                                <h4>ALAMAT</h4>
                                <ul class="list">
                                    <li style="color: white;">Gedung Bappebti Lt. 1-7, Jl. Kramat Raya No. 172 Jakarta 10430</li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-xl-3 col-lg-3 col-md-3">
                            <div class="widget">
                                <h4>KONTAK</h4>
                                <ul class="list">
                                    <li style="color: white;"><i class="fas fa-phone-alt"></i> 0811 1109 901</li>
                                    <li style="color: white;"><i class="fa fa-comment-dots"></i> (021) 3192 4744</li>
                                    <li style="color: white;"><i class="fa fa-fax"></i> (021) 3192 3204</li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-xl-2 col-lg-2 col-md-3">
                            <div class="widget">
                                <h4>SOCIAL MEDIA</h4>
                                <div class="social-icons">
                                    <ul>
                                        <li class="social-facebook">
											<a href="http://www.facebook.com/bappebti.kementerianperdagangan" target="blank"><i class="fab fa-facebook-f"></i></a>
										</li>
                                        <li class="social-twitter">
											<a href="http://twitter.com/InfoBappebti" target="blank"><i class="fab fa-twitter"></i></a>
										</li>
                                        <li class="social-twitter">
											<a href="http://www.instagram.com/bappebti/" target="blank"><i class="fab fa-instagram"></i></a>
										</li>
                                        <li class="social-youtube">
											<a href="#" target="blank"><i class="fab fa-youtube"></i></a>
										</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 text-end">
                            <div class="copyright-text" style="color: white; text-align: center;"><strong style="font-size: 12px;">COPYRIGHT © BAPPEBTI - 2021</strong></div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end: FOOTER -->
    </div>
    <!-- end: BODY INNER -->

    <a id="scrollTop"><i class="icon-chevron-up"></i><i class="icon-chevron-up"></i></a>
    <script src="<?php echo base_url('assets/js/jquery.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/plugins.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/functions.js') ?>"></script>
    <script type='text/javascript' src='//maps.googleapis.com/maps/api/js?key=AIzaSyBOksKHb9HyydVB-mcrqKUVfA_LeB79jcQ'></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/gmap3/gmap3.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/gmap3/map-styles.js') ?>"></script>
</body>

</html>
