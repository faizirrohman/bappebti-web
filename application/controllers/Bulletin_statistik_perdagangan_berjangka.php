<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Bulletin_statistik_perdagangan_berjangka extends CI_Controller {
	public function index()
	{
		$data['title'] = "Bulletin Statistik Perdagangan Berjangka | Kementerian Perdagangan Republik Indonesia";
		$this->load->view('public/bulletin_statistik_perdagangan_berjangka/index', $data);
	}
}
