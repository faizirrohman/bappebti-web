<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Siaran_pers extends CI_Controller {
	public function index()
	{
		$data['title'] = "Siaran Pers Bappebti | Kementerian Perdagangan Republik Indonesia";
		$this->load->view('public/siaran_pers/index', $data);
	}

	public function detail()
	{
		$data['title'] = 'Detail Siaran Pers | Kementerian Perdagangan Republik Indonesia';
		$this->load->view('public/siaran_pers/detail', $data);
	}
}
