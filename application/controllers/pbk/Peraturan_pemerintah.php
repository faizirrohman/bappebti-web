<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Peraturan_pemerintah extends CI_Controller {
	public function index()
	{
		$data['title'] = "Peraturan Pemerintah | Kementerian Perdagangan Republik Indonesia";
		$this->load->view('public/peraturan_pemerintah/index', $data);
	}

	public function detail()
	{
		$data['title'] = "Detail Peraturan Pemerintah | Kementerian Perdagangan Republik Indonesia";
		$this->load->view('public/peraturan_pemerintah/detail', $data);
	}
}
