<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Undang_undang extends CI_Controller {
	public function index()
	{
		$data['title'] = "Undang-undang | Kementerian Perdagangan Republik Indonesia";
		$this->load->view('public/undang_undang/index', $data);
	}

	public function detail()
	{
		$data['title'] = "Detail Undang-undang | Kementerian Perdagangan Republik Indonesia";
		$this->load->view('public/undang_undang/detail', $data);
	}
}
