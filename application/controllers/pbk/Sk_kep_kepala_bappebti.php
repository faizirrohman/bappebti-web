<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sk_kep_kepala_bappebti extends CI_Controller {
	public function index()
	{
		$data['title'] = "Sk Kep Kepala Bappebti | Kementerian Perdagangan Republik Indonesia";
		$this->load->view('public/sk_kep_kepala_bappebti/index', $data);
	}

	public function detail()
	{
		$data['title'] = "Detail Sk Kep Kepala Bappebti | Kementerian Perdagangan Republik Indonesia";
		$this->load->view('public/sk_kep_kepala_bappebti/detail', $data);
	}
}
