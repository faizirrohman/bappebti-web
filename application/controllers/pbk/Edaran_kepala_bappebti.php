<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Edaran_kepala_bappebti extends CI_Controller {
	public function index()
	{
		$data['title'] = "Edaran Kepala Bappebti | Kementerian Perdagangan Republik Indonesia";
		$this->load->view('public/edaran_kepala_bappebti/index', $data);
	}

	public function detail()
	{
		$data['title'] = "Detail Edaran Kepala Bappebti | Kementerian Perdagangan Republik Indonesia";
		$this->load->view('public/edaran_kepala_bappebti/detail', $data);
	}
}
