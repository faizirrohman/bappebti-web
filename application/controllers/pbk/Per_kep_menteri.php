<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Per_kep_menteri extends CI_Controller {
	public function index()
	{
		$data['title'] = "Undang-undang | Kementerian Perdagangan Republik Indonesia";
		$this->load->view('public/per_kep_menteri/index', $data);
	}

	public function detail()
	{
		$data['title'] = "Detail Undang-undang | Kementerian Perdagangan Republik Indonesia";
		$this->load->view('public/per_kep_menteri/detail', $data);
	}
}
