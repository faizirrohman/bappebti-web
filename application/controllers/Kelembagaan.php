<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kelembagaan extends CI_Controller {
	public function index()
	{
		$data['title'] = "Kelembagaan | Kementerian Perdagangan Republik Indonesia";
		$this->load->view('public/kelembagaan/index', $data);
	}
}
