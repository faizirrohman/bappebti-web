<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Organisasi extends CI_Controller {
	public function index()
	{
		$data['title'] = "Organisasi | Kementerian Perdagangan Republik Indonesia";
		$this->load->view('public/organisasi/index', $data);
	}
}
