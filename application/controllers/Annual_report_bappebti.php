<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Annual_report_bappebti extends CI_Controller {
	public function index()
	{
		$data['title'] = "Bappebti | Kementerian Perdagangan Republik Indonesia";
		$this->load->view('public/annual_report_bappebti/index', $data);
	}
}
