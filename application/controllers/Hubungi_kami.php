<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hubungi_kami extends CI_Controller {
	public function index()
	{
		$data['title'] = "Kontak | Kementerian Perdagangan Republik Indonesia";
		$this->load->view('public/hubungi_kami/index', $data);
	}
}
