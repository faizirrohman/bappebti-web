<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Aktualita extends CI_Controller {
	public function index()
	{
		$data['title'] = "Bappebti | Kementerian Perdagangan Republik Indonesia";
		$this->load->view('public/aktualita/index', $data);
	}

	public function detail()
	{
		$data['title'] = "Bappebti | Kementerian Perdagangan Republik Indonesia";
		$this->load->view('public/aktualita/detail', $data);
	}
}
