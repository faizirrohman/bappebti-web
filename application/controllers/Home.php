<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function index()
	{
		$data['title'] = "Bappebti | Kementerian Perdagangan Republik Indonesia";
		$this->load->view('public/home/index', $data);
	}
}
