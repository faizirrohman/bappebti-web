<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kewenangan extends CI_Controller {
	public function index()
	{
		$data['title'] = "Kewenangan | Kementerian Perdagangan Republik Indonesia";
		$this->load->view('public/kewenangan/index', $data);
	}
}
